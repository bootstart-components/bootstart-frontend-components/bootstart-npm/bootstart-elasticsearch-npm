/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class EsEntityAdminComponent {
    constructor() {
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.esAdminService.refreshSummary();
    }
}
EsEntityAdminComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-es-entity-admin',
                template: `<div class="es-entity-admin-menu-container">
  <h2>{{title}}</h2>

  <mat-list>
    <mat-list-item *ngFor="let indexSummary of esAdminService.summary" class="row"
                   [ngClass]="{'index-with-alias': indexSummary.withAlias}">
      <div class="col-sm-auto">
        <i class="fas fa-tag" *ngIf="indexSummary.withAlias"></i> &nbsp;
        <span>Version {{indexSummary.version}}</span>
      </div>
      <div class="col-sm-auto">
        <span>{{indexSummary.docCount}} documents</span>
      </div>
      <button mat-button color="warn" class="col-sm-auto"
              title="Delete index"
              (click)="esAdminService.deleteIndex(indexSummary.name)">
        <i class="fas fa-trash"></i>
      </button>
      <button mat-button color="primary"
              class="col-sm-auto"
              *ngIf="!indexSummary.withAlias"
              title="Switch Alias"
              (click)="esAdminService.switchAlias(indexSummary.name)">
        <i class="fas fa-tag"></i>
      </button>
    </mat-list-item>
  </mat-list>

  <div class="es-entity-admin-button-row">
    <button mat-raised-button color="raised-info" (click)="esAdminService.setupTemplate()">
      <i class="fas fa-book"></i> &nbsp; Setup Template
    </button>
    <button mat-raised-button color="raised-warning" (click)="esAdminService.updateVersion()">
      <i class="fas fa-arrow-up"></i> &nbsp; Update Version
    </button>
  </div>

</div>
`,
                styles: [`.es-entity-admin-menu-container{margin-top:40px}.es-entity-admin-menu-container h2{color:#000;border-bottom:none}.es-entity-admin-menu-container .index-with-alias{font-weight:700;border-bottom:none}.es-entity-admin-button-row{text-align:center}.es-entity-admin-button-row button{margin-right:5px}`]
            },] },
];
/** @nocollapse */
EsEntityAdminComponent.ctorParameters = () => [];
EsEntityAdminComponent.propDecorators = {
    esAdminService: [{ type: Input }],
    title: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    EsEntityAdminComponent.prototype.esAdminService;
    /** @type {?} */
    EsEntityAdminComponent.prototype.title;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXMtZW50aXR5LWFkbWluLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1lbGFzdGljc2VhcmNoLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZXMtZW50aXR5LWFkbWluL2VzLWVudGl0eS1hZG1pbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBOEN2RCxNQUFNO0lBS0o7S0FDQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsRUFBRSxDQUFDO0tBQ3RDOzs7WUFyREYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSwyQkFBMkI7Z0JBQ3JDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0FzQ1g7Z0JBQ0MsTUFBTSxFQUFFLENBQUMsMFNBQTBTLENBQUM7YUFDclQ7Ozs7OzZCQUdFLEtBQUs7b0JBQ0wsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7RXNBZG1pblNlcnZpY2V9IGZyb20gJy4uLy4uL21vZGVscy9lcy1hZG1pbi1zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYm9vdHN0YXJ0LWVzLWVudGl0eS1hZG1pbicsXG4gIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cImVzLWVudGl0eS1hZG1pbi1tZW51LWNvbnRhaW5lclwiPlxuICA8aDI+e3t0aXRsZX19PC9oMj5cblxuICA8bWF0LWxpc3Q+XG4gICAgPG1hdC1saXN0LWl0ZW0gKm5nRm9yPVwibGV0IGluZGV4U3VtbWFyeSBvZiBlc0FkbWluU2VydmljZS5zdW1tYXJ5XCIgY2xhc3M9XCJyb3dcIlxuICAgICAgICAgICAgICAgICAgIFtuZ0NsYXNzXT1cInsnaW5kZXgtd2l0aC1hbGlhcyc6IGluZGV4U3VtbWFyeS53aXRoQWxpYXN9XCI+XG4gICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLWF1dG9cIj5cbiAgICAgICAgPGkgY2xhc3M9XCJmYXMgZmEtdGFnXCIgKm5nSWY9XCJpbmRleFN1bW1hcnkud2l0aEFsaWFzXCI+PC9pPiAmbmJzcDtcbiAgICAgICAgPHNwYW4+VmVyc2lvbiB7e2luZGV4U3VtbWFyeS52ZXJzaW9ufX08L3NwYW4+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tYXV0b1wiPlxuICAgICAgICA8c3Bhbj57e2luZGV4U3VtbWFyeS5kb2NDb3VudH19IGRvY3VtZW50czwvc3Bhbj5cbiAgICAgIDwvZGl2PlxuICAgICAgPGJ1dHRvbiBtYXQtYnV0dG9uIGNvbG9yPVwid2FyblwiIGNsYXNzPVwiY29sLXNtLWF1dG9cIlxuICAgICAgICAgICAgICB0aXRsZT1cIkRlbGV0ZSBpbmRleFwiXG4gICAgICAgICAgICAgIChjbGljayk9XCJlc0FkbWluU2VydmljZS5kZWxldGVJbmRleChpbmRleFN1bW1hcnkubmFtZSlcIj5cbiAgICAgICAgPGkgY2xhc3M9XCJmYXMgZmEtdHJhc2hcIj48L2k+XG4gICAgICA8L2J1dHRvbj5cbiAgICAgIDxidXR0b24gbWF0LWJ1dHRvbiBjb2xvcj1cInByaW1hcnlcIlxuICAgICAgICAgICAgICBjbGFzcz1cImNvbC1zbS1hdXRvXCJcbiAgICAgICAgICAgICAgKm5nSWY9XCIhaW5kZXhTdW1tYXJ5LndpdGhBbGlhc1wiXG4gICAgICAgICAgICAgIHRpdGxlPVwiU3dpdGNoIEFsaWFzXCJcbiAgICAgICAgICAgICAgKGNsaWNrKT1cImVzQWRtaW5TZXJ2aWNlLnN3aXRjaEFsaWFzKGluZGV4U3VtbWFyeS5uYW1lKVwiPlxuICAgICAgICA8aSBjbGFzcz1cImZhcyBmYS10YWdcIj48L2k+XG4gICAgICA8L2J1dHRvbj5cbiAgICA8L21hdC1saXN0LWl0ZW0+XG4gIDwvbWF0LWxpc3Q+XG5cbiAgPGRpdiBjbGFzcz1cImVzLWVudGl0eS1hZG1pbi1idXR0b24tcm93XCI+XG4gICAgPGJ1dHRvbiBtYXQtcmFpc2VkLWJ1dHRvbiBjb2xvcj1cInJhaXNlZC1pbmZvXCIgKGNsaWNrKT1cImVzQWRtaW5TZXJ2aWNlLnNldHVwVGVtcGxhdGUoKVwiPlxuICAgICAgPGkgY2xhc3M9XCJmYXMgZmEtYm9va1wiPjwvaT4gJm5ic3A7IFNldHVwIFRlbXBsYXRlXG4gICAgPC9idXR0b24+XG4gICAgPGJ1dHRvbiBtYXQtcmFpc2VkLWJ1dHRvbiBjb2xvcj1cInJhaXNlZC13YXJuaW5nXCIgKGNsaWNrKT1cImVzQWRtaW5TZXJ2aWNlLnVwZGF0ZVZlcnNpb24oKVwiPlxuICAgICAgPGkgY2xhc3M9XCJmYXMgZmEtYXJyb3ctdXBcIj48L2k+ICZuYnNwOyBVcGRhdGUgVmVyc2lvblxuICAgIDwvYnV0dG9uPlxuICA8L2Rpdj5cblxuPC9kaXY+XG5gLFxuICBzdHlsZXM6IFtgLmVzLWVudGl0eS1hZG1pbi1tZW51LWNvbnRhaW5lcnttYXJnaW4tdG9wOjQwcHh9LmVzLWVudGl0eS1hZG1pbi1tZW51LWNvbnRhaW5lciBoMntjb2xvcjojMDAwO2JvcmRlci1ib3R0b206bm9uZX0uZXMtZW50aXR5LWFkbWluLW1lbnUtY29udGFpbmVyIC5pbmRleC13aXRoLWFsaWFze2ZvbnQtd2VpZ2h0OjcwMDtib3JkZXItYm90dG9tOm5vbmV9LmVzLWVudGl0eS1hZG1pbi1idXR0b24tcm93e3RleHQtYWxpZ246Y2VudGVyfS5lcy1lbnRpdHktYWRtaW4tYnV0dG9uLXJvdyBidXR0b257bWFyZ2luLXJpZ2h0OjVweH1gXVxufSlcbmV4cG9ydCBjbGFzcyBFc0VudGl0eUFkbWluQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBlc0FkbWluU2VydmljZTogRXNBZG1pblNlcnZpY2U7XG4gIEBJbnB1dCgpIHRpdGxlOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLmVzQWRtaW5TZXJ2aWNlLnJlZnJlc2hTdW1tYXJ5KCk7XG4gIH1cbn1cbiJdfQ==
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { HttpParams } from '@angular/common/http';
export class DefaultEsAdminService {
    /**
     * @param {?} _http
     * @param {?} baseURl
     * @param {?} entityName
     */
    constructor(_http, baseURl, entityName) {
        this.http = _http;
        this.BASE_URL = baseURl;
        this.ENTITY_NAME = entityName;
    }
    /**
     * @return {?}
     */
    refreshSummary() {
        this.http.get(this.BASE_URL + 'indices-summary', {}).subscribe(response => this.summary = response.sort(function (a, b) {
            return a.version - b.version;
        }), () => console.error('Unable to retrieve index summary for ' + this.ENTITY_NAME));
    }
    /**
     * @return {?}
     */
    setupTemplate() {
        this.http.post(this.BASE_URL + 'setup-index-template', {}, {}).subscribe(() => {
            this.refreshSummary();
            console.info('Mapping template successfully setup for ' + this.ENTITY_NAME);
        }, () => {
            console.error('Unable to setup mapping template for ' + this.ENTITY_NAME);
        });
    }
    /**
     * @return {?}
     */
    updateVersion() {
        this.http.post(this.BASE_URL + 'update-index-version', {}, {}).subscribe(() => {
            console.info('Updated version of ' + this.ENTITY_NAME + ' index');
            setTimeout(() => this.refreshSummary(), 500);
        }, () => {
            console.error('Unable to update index version for ' + this.ENTITY_NAME);
        });
    }
    /**
     * @param {?} target
     * @return {?}
     */
    switchAlias(target) {
        this.http.post(this.BASE_URL + 'switch-index-alias', {}, {
            params: new HttpParams().append('esIndexName', target)
        }).subscribe(() => this.refreshSummary(), () => {
            console.error('Unable to switch alias to index ' + target);
        });
    }
    /**
     * @param {?} target
     * @return {?}
     */
    deleteIndex(target) {
        this.http.post(this.BASE_URL + 'delete-index', {}, {
            params: new HttpParams().append('esIndexName', target)
        }).subscribe(() => this.refreshSummary(), () => {
            console.error('Unable to delete index ' + target);
        });
    }
}
if (false) {
    /** @type {?} */
    DefaultEsAdminService.prototype.http;
    /** @type {?} */
    DefaultEsAdminService.prototype.BASE_URL;
    /** @type {?} */
    DefaultEsAdminService.prototype.ENTITY_NAME;
    /** @type {?} */
    DefaultEsAdminService.prototype.summary;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVmYXVsdC1lcy1hZG1pbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2RlZmF1bHQtZXMtYWRtaW4uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFhLFVBQVUsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBSzVELE1BQU07Ozs7OztJQU9KLFlBQVksS0FBaUIsRUFBRSxPQUFlLEVBQUUsVUFBa0I7UUFDaEUsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7UUFDbEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7UUFDeEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUM7S0FDL0I7Ozs7SUFHTSxjQUFjO1FBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUNYLElBQUksQ0FBQyxRQUFRLEdBQUcsaUJBQWlCLEVBQ2pDLEVBQUUsQ0FDSCxDQUFDLFNBQVMsQ0FDVCxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO1lBQ3JELE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUM7U0FDOUIsQ0FBQyxFQUNGLEdBQUcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsdUNBQXVDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUNoRixDQUFDOzs7OztJQUdHLGFBQWE7UUFDbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQ1osSUFBSSxDQUFDLFFBQVEsR0FBRyxzQkFBc0IsRUFDdEMsRUFBRSxFQUNGLEVBQUUsQ0FDSCxDQUFDLFNBQVMsQ0FDVCxHQUFHLEVBQUU7WUFDSCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdEIsT0FBTyxDQUFDLElBQUksQ0FBQywwQ0FBMEMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7U0FDN0UsRUFDRCxHQUFHLEVBQUU7WUFDSCxPQUFPLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztTQUMzRSxDQUNGLENBQUM7Ozs7O0lBR0csYUFBYTtRQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDWixJQUFJLENBQUMsUUFBUSxHQUFHLHNCQUFzQixFQUN0QyxFQUFFLEVBQ0YsRUFBRSxDQUNILENBQUMsU0FBUyxDQUNULEdBQUcsRUFBRTtZQUNILE9BQU8sQ0FBQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUMsQ0FBQztZQUNsRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQzlDLEVBQ0QsR0FBRyxFQUFFO1lBQ0gsT0FBTyxDQUFDLEtBQUssQ0FBQyxxQ0FBcUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7U0FDekUsQ0FDRixDQUFDOzs7Ozs7SUFHRyxXQUFXLENBQUMsTUFBYztRQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDWixJQUFJLENBQUMsUUFBUSxHQUFHLG9CQUFvQixFQUNwQyxFQUFFLEVBQ0Y7WUFDRSxNQUFNLEVBQUUsSUFBSSxVQUFVLEVBQUUsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQztTQUN2RCxDQUNGLENBQUMsU0FBUyxDQUNULEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsRUFDM0IsR0FBRyxFQUFFO1lBQ0gsT0FBTyxDQUFDLEtBQUssQ0FBQyxrQ0FBa0MsR0FBRyxNQUFNLENBQUMsQ0FBQztTQUM1RCxDQUNGLENBQUM7Ozs7OztJQUdHLFdBQVcsQ0FBQyxNQUFjO1FBQy9CLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUNaLElBQUksQ0FBQyxRQUFRLEdBQUcsY0FBYyxFQUM5QixFQUFFLEVBQ0Y7WUFDRSxNQUFNLEVBQUUsSUFBSSxVQUFVLEVBQUUsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQztTQUN2RCxDQUNGLENBQUMsU0FBUyxDQUNULEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsRUFDM0IsR0FBRyxFQUFFO1lBQ0gsT0FBTyxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsR0FBRyxNQUFNLENBQUMsQ0FBQztTQUNuRCxDQUNGLENBQUM7O0NBR0wiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0h0dHBDbGllbnQsIEh0dHBQYXJhbXN9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7RXNBZG1pblNlcnZpY2V9IGZyb20gJy4vZXMtYWRtaW4tc2VydmljZSc7XG5pbXBvcnQge0VzSW5kZXhTdW1tYXJ5fSBmcm9tICcuL2VzLWluZGV4LXN1bW1hcnknO1xuXG5cbmV4cG9ydCBjbGFzcyBEZWZhdWx0RXNBZG1pblNlcnZpY2UgaW1wbGVtZW50cyBFc0FkbWluU2VydmljZSB7XG5cbiAgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50O1xuICBCQVNFX1VSTDogc3RyaW5nO1xuICBFTlRJVFlfTkFNRTogc3RyaW5nO1xuICBzdW1tYXJ5OiBFc0luZGV4U3VtbWFyeVtdO1xuXG4gIGNvbnN0cnVjdG9yKF9odHRwOiBIdHRwQ2xpZW50LCBiYXNlVVJsOiBzdHJpbmcsIGVudGl0eU5hbWU6IHN0cmluZykge1xuICAgIHRoaXMuaHR0cCA9IF9odHRwO1xuICAgIHRoaXMuQkFTRV9VUkwgPSBiYXNlVVJsO1xuICAgIHRoaXMuRU5USVRZX05BTUUgPSBlbnRpdHlOYW1lO1xuICB9XG5cblxuICBwdWJsaWMgcmVmcmVzaFN1bW1hcnkoKTogdm9pZCB7XG4gICAgdGhpcy5odHRwLmdldDxFc0luZGV4U3VtbWFyeVtdPihcbiAgICAgIHRoaXMuQkFTRV9VUkwgKyAnaW5kaWNlcy1zdW1tYXJ5JyxcbiAgICAgIHt9XG4gICAgKS5zdWJzY3JpYmUoXG4gICAgICByZXNwb25zZSA9PiB0aGlzLnN1bW1hcnkgPSByZXNwb25zZS5zb3J0KGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgICAgIHJldHVybiBhLnZlcnNpb24gLSBiLnZlcnNpb247XG4gICAgICB9KSxcbiAgICAgICgpID0+IGNvbnNvbGUuZXJyb3IoJ1VuYWJsZSB0byByZXRyaWV2ZSBpbmRleCBzdW1tYXJ5IGZvciAnICsgdGhpcy5FTlRJVFlfTkFNRSlcbiAgICApO1xuICB9XG5cbiAgcHVibGljIHNldHVwVGVtcGxhdGUoKTogdm9pZCB7XG4gICAgdGhpcy5odHRwLnBvc3QoXG4gICAgICB0aGlzLkJBU0VfVVJMICsgJ3NldHVwLWluZGV4LXRlbXBsYXRlJyxcbiAgICAgIHt9LFxuICAgICAge31cbiAgICApLnN1YnNjcmliZShcbiAgICAgICgpID0+IHtcbiAgICAgICAgdGhpcy5yZWZyZXNoU3VtbWFyeSgpO1xuICAgICAgICBjb25zb2xlLmluZm8oJ01hcHBpbmcgdGVtcGxhdGUgc3VjY2Vzc2Z1bGx5IHNldHVwIGZvciAnICsgdGhpcy5FTlRJVFlfTkFNRSk7XG4gICAgICB9LFxuICAgICAgKCkgPT4ge1xuICAgICAgICBjb25zb2xlLmVycm9yKCdVbmFibGUgdG8gc2V0dXAgbWFwcGluZyB0ZW1wbGF0ZSBmb3IgJyArIHRoaXMuRU5USVRZX05BTUUpO1xuICAgICAgfVxuICAgICk7XG4gIH1cblxuICBwdWJsaWMgdXBkYXRlVmVyc2lvbigpOiB2b2lkIHtcbiAgICB0aGlzLmh0dHAucG9zdChcbiAgICAgIHRoaXMuQkFTRV9VUkwgKyAndXBkYXRlLWluZGV4LXZlcnNpb24nLFxuICAgICAge30sXG4gICAgICB7fVxuICAgICkuc3Vic2NyaWJlKFxuICAgICAgKCkgPT4ge1xuICAgICAgICBjb25zb2xlLmluZm8oJ1VwZGF0ZWQgdmVyc2lvbiBvZiAnICsgdGhpcy5FTlRJVFlfTkFNRSArICcgaW5kZXgnKTtcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLnJlZnJlc2hTdW1tYXJ5KCksIDUwMCk7XG4gICAgICB9LFxuICAgICAgKCkgPT4ge1xuICAgICAgICBjb25zb2xlLmVycm9yKCdVbmFibGUgdG8gdXBkYXRlIGluZGV4IHZlcnNpb24gZm9yICcgKyB0aGlzLkVOVElUWV9OQU1FKTtcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbiAgcHVibGljIHN3aXRjaEFsaWFzKHRhcmdldDogc3RyaW5nKSB7XG4gICAgdGhpcy5odHRwLnBvc3QoXG4gICAgICB0aGlzLkJBU0VfVVJMICsgJ3N3aXRjaC1pbmRleC1hbGlhcycsXG4gICAgICB7fSxcbiAgICAgIHtcbiAgICAgICAgcGFyYW1zOiBuZXcgSHR0cFBhcmFtcygpLmFwcGVuZCgnZXNJbmRleE5hbWUnLCB0YXJnZXQpXG4gICAgICB9XG4gICAgKS5zdWJzY3JpYmUoXG4gICAgICAoKSA9PiB0aGlzLnJlZnJlc2hTdW1tYXJ5KCksXG4gICAgICAoKSA9PiB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ1VuYWJsZSB0byBzd2l0Y2ggYWxpYXMgdG8gaW5kZXggJyArIHRhcmdldCk7XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIHB1YmxpYyBkZWxldGVJbmRleCh0YXJnZXQ6IHN0cmluZykge1xuICAgIHRoaXMuaHR0cC5wb3N0KFxuICAgICAgdGhpcy5CQVNFX1VSTCArICdkZWxldGUtaW5kZXgnLFxuICAgICAge30sXG4gICAgICB7XG4gICAgICAgIHBhcmFtczogbmV3IEh0dHBQYXJhbXMoKS5hcHBlbmQoJ2VzSW5kZXhOYW1lJywgdGFyZ2V0KVxuICAgICAgfVxuICAgICkuc3Vic2NyaWJlKFxuICAgICAgKCkgPT4gdGhpcy5yZWZyZXNoU3VtbWFyeSgpLFxuICAgICAgKCkgPT4ge1xuICAgICAgICBjb25zb2xlLmVycm9yKCdVbmFibGUgdG8gZGVsZXRlIGluZGV4ICcgKyB0YXJnZXQpO1xuICAgICAgfVxuICAgICk7XG4gIH1cblxufVxuIl19
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function EsAdminService() { }
/** @type {?} */
EsAdminService.prototype.BASE_URL;
/** @type {?} */
EsAdminService.prototype.ENTITY_NAME;
/**
 * List of Elasticsearch indices linked to the current entity
 * @type {?}
 */
EsAdminService.prototype.summary;
/**
 * Send request to refresh the list of Elasticsearch indices
 * @type {?}
 */
EsAdminService.prototype.refreshSummary;
/**
 * Send request to setup mapping template for the current entity
 * @type {?}
 */
EsAdminService.prototype.setupTemplate;
/**
 * Send request to update the index version for the current entity
 * @type {?}
 */
EsAdminService.prototype.updateVersion;
/**
 * Send request to switch entity alias to a target index
 * @type {?}
 */
EsAdminService.prototype.switchAlias;
/**
 * Send request to delete a given index
 * @type {?}
 */
EsAdminService.prototype.deleteIndex;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXMtYWRtaW4tc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1lbGFzdGljc2VhcmNoLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9lcy1hZG1pbi1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0VzSW5kZXhTdW1tYXJ5fSBmcm9tICcuL2VzLWluZGV4LXN1bW1hcnknO1xuXG5leHBvcnQgaW50ZXJmYWNlIEVzQWRtaW5TZXJ2aWNlIHtcblxuICBCQVNFX1VSTDogc3RyaW5nO1xuICBFTlRJVFlfTkFNRTogc3RyaW5nO1xuXG4gIC8qKiBMaXN0IG9mIEVsYXN0aWNzZWFyY2ggaW5kaWNlcyBsaW5rZWQgdG8gdGhlIGN1cnJlbnQgZW50aXR5ICovXG4gIHN1bW1hcnk6IEVzSW5kZXhTdW1tYXJ5W107XG5cbiAgLyoqIFNlbmQgcmVxdWVzdCB0byByZWZyZXNoIHRoZSBsaXN0IG9mIEVsYXN0aWNzZWFyY2ggaW5kaWNlcyAqL1xuICByZWZyZXNoU3VtbWFyeSgpOiB2b2lkO1xuXG4gIC8qKiBTZW5kIHJlcXVlc3QgdG8gc2V0dXAgbWFwcGluZyB0ZW1wbGF0ZSBmb3IgdGhlIGN1cnJlbnQgZW50aXR5ICovXG4gIHNldHVwVGVtcGxhdGUoKTogdm9pZDtcblxuICAvKiogU2VuZCByZXF1ZXN0IHRvIHVwZGF0ZSB0aGUgaW5kZXggdmVyc2lvbiBmb3IgdGhlIGN1cnJlbnQgZW50aXR5ICovXG4gIHVwZGF0ZVZlcnNpb24oKTogdm9pZFxuXG4gIC8qKiBTZW5kIHJlcXVlc3QgdG8gc3dpdGNoIGVudGl0eSBhbGlhcyB0byBhIHRhcmdldCBpbmRleCAqL1xuICBzd2l0Y2hBbGlhcyh0YXJnZXQ6IHN0cmluZyk6IHZvaWQ7XG5cbiAgLyoqIFNlbmQgcmVxdWVzdCB0byBkZWxldGUgYSBnaXZlbiBpbmRleCAqL1xuICBkZWxldGVJbmRleCh0YXJnZXQ6IHN0cmluZyk6IHZvaWQ7XG5cbn1cbiJdfQ==
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { EsEntityAdminComponent } from './components/es-entity-admin/es-entity-admin.component';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatListModule } from '@angular/material';
export class BootstartElasticsearchModule {
}
BootstartElasticsearchModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    MatListModule,
                    MatButtonModule
                ],
                declarations: [
                    EsEntityAdminComponent
                ],
                exports: [
                    EsEntityAdminComponent
                ]
            },] },
];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gvIiwic291cmNlcyI6WyJsaWIvYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLHdEQUF3RCxDQUFDO0FBQzlGLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsZUFBZSxFQUFFLGFBQWEsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBZWpFLE1BQU07OztZQWJMLFFBQVEsU0FBQztnQkFDRSxPQUFPLEVBQU87b0JBQ1osWUFBWTtvQkFDWixhQUFhO29CQUNiLGVBQWU7aUJBQ2hCO2dCQUNELFlBQVksRUFBRTtvQkFDWixzQkFBc0I7aUJBQ3ZCO2dCQUNELE9BQU8sRUFBTztvQkFDWixzQkFBc0I7aUJBQ3ZCO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7RXNFbnRpdHlBZG1pbkNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2VzLWVudGl0eS1hZG1pbi9lcy1lbnRpdHktYWRtaW4uY29tcG9uZW50JztcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtNYXRCdXR0b25Nb2R1bGUsIE1hdExpc3RNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcblxuQE5nTW9kdWxlKHtcbiAgICAgICAgICAgIGltcG9ydHMgICAgIDogW1xuICAgICAgICAgICAgICBDb21tb25Nb2R1bGUsXG4gICAgICAgICAgICAgIE1hdExpc3RNb2R1bGUsXG4gICAgICAgICAgICAgIE1hdEJ1dHRvbk1vZHVsZVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGRlY2xhcmF0aW9uczogW1xuICAgICAgICAgICAgICBFc0VudGl0eUFkbWluQ29tcG9uZW50XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZXhwb3J0cyAgICAgOiBbXG4gICAgICAgICAgICAgIEVzRW50aXR5QWRtaW5Db21wb25lbnRcbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEVsYXN0aWNzZWFyY2hNb2R1bGUge1xufVxuIl19
export interface EsIndexSummary {
    name: string;
    version: number;
    withAlias: boolean;
    docCount: number;
}

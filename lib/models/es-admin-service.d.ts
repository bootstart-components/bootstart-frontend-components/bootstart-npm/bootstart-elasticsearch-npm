import { EsIndexSummary } from './es-index-summary';
export interface EsAdminService {
    BASE_URL: string;
    ENTITY_NAME: string;
    /** List of Elasticsearch indices linked to the current entity */
    summary: EsIndexSummary[];
    /** Send request to refresh the list of Elasticsearch indices */
    refreshSummary(): void;
    /** Send request to setup mapping template for the current entity */
    setupTemplate(): void;
    /** Send request to update the index version for the current entity */
    updateVersion(): void;
    /** Send request to switch entity alias to a target index */
    switchAlias(target: string): void;
    /** Send request to delete a given index */
    deleteIndex(target: string): void;
}

import { HttpClient } from '@angular/common/http';
import { EsAdminService } from './es-admin-service';
import { EsIndexSummary } from './es-index-summary';
export declare class DefaultEsAdminService implements EsAdminService {
    private http;
    BASE_URL: string;
    ENTITY_NAME: string;
    summary: EsIndexSummary[];
    constructor(_http: HttpClient, baseURl: string, entityName: string);
    refreshSummary(): void;
    setupTemplate(): void;
    updateVersion(): void;
    switchAlias(target: string): void;
    deleteIndex(target: string): void;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
var EsEntityAdminComponent = /** @class */ (function () {
    function EsEntityAdminComponent() {
    }
    /**
     * @return {?}
     */
    EsEntityAdminComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.esAdminService.refreshSummary();
    };
    EsEntityAdminComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-es-entity-admin',
                    template: "<div class=\"es-entity-admin-menu-container\">\n  <h2>{{title}}</h2>\n\n  <mat-list>\n    <mat-list-item *ngFor=\"let indexSummary of esAdminService.summary\" class=\"row\"\n                   [ngClass]=\"{'index-with-alias': indexSummary.withAlias}\">\n      <div class=\"col-sm-auto\">\n        <i class=\"fas fa-tag\" *ngIf=\"indexSummary.withAlias\"></i> &nbsp;\n        <span>Version {{indexSummary.version}}</span>\n      </div>\n      <div class=\"col-sm-auto\">\n        <span>{{indexSummary.docCount}} documents</span>\n      </div>\n      <button mat-button color=\"warn\" class=\"col-sm-auto\"\n              title=\"Delete index\"\n              (click)=\"esAdminService.deleteIndex(indexSummary.name)\">\n        <i class=\"fas fa-trash\"></i>\n      </button>\n      <button mat-button color=\"primary\"\n              class=\"col-sm-auto\"\n              *ngIf=\"!indexSummary.withAlias\"\n              title=\"Switch Alias\"\n              (click)=\"esAdminService.switchAlias(indexSummary.name)\">\n        <i class=\"fas fa-tag\"></i>\n      </button>\n    </mat-list-item>\n  </mat-list>\n\n  <div class=\"es-entity-admin-button-row\">\n    <button mat-raised-button color=\"raised-info\" (click)=\"esAdminService.setupTemplate()\">\n      <i class=\"fas fa-book\"></i> &nbsp; Setup Template\n    </button>\n    <button mat-raised-button color=\"raised-warning\" (click)=\"esAdminService.updateVersion()\">\n      <i class=\"fas fa-arrow-up\"></i> &nbsp; Update Version\n    </button>\n  </div>\n\n</div>\n",
                    styles: [".es-entity-admin-menu-container{margin-top:40px}.es-entity-admin-menu-container h2{color:#000;border-bottom:none}.es-entity-admin-menu-container .index-with-alias{font-weight:700;border-bottom:none}.es-entity-admin-button-row{text-align:center}.es-entity-admin-button-row button{margin-right:5px}"]
                },] },
    ];
    /** @nocollapse */
    EsEntityAdminComponent.ctorParameters = function () { return []; };
    EsEntityAdminComponent.propDecorators = {
        esAdminService: [{ type: Input }],
        title: [{ type: Input }]
    };
    return EsEntityAdminComponent;
}());
export { EsEntityAdminComponent };
if (false) {
    /** @type {?} */
    EsEntityAdminComponent.prototype.esAdminService;
    /** @type {?} */
    EsEntityAdminComponent.prototype.title;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXMtZW50aXR5LWFkbWluLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1lbGFzdGljc2VhcmNoLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZXMtZW50aXR5LWFkbWluL2VzLWVudGl0eS1hZG1pbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDOztJQW1EckQ7S0FDQzs7OztJQUVELHlDQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxFQUFFLENBQUM7S0FDdEM7O2dCQXJERixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsUUFBUSxFQUFFLHcvQ0FzQ1g7b0JBQ0MsTUFBTSxFQUFFLENBQUMsMFNBQTBTLENBQUM7aUJBQ3JUOzs7OztpQ0FHRSxLQUFLO3dCQUNMLEtBQUs7O2lDQWpEUjs7U0E4Q2Esc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtFc0FkbWluU2VydmljZX0gZnJvbSAnLi4vLi4vbW9kZWxzL2VzLWFkbWluLXNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdib290c3RhcnQtZXMtZW50aXR5LWFkbWluJyxcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwiZXMtZW50aXR5LWFkbWluLW1lbnUtY29udGFpbmVyXCI+XG4gIDxoMj57e3RpdGxlfX08L2gyPlxuXG4gIDxtYXQtbGlzdD5cbiAgICA8bWF0LWxpc3QtaXRlbSAqbmdGb3I9XCJsZXQgaW5kZXhTdW1tYXJ5IG9mIGVzQWRtaW5TZXJ2aWNlLnN1bW1hcnlcIiBjbGFzcz1cInJvd1wiXG4gICAgICAgICAgICAgICAgICAgW25nQ2xhc3NdPVwieydpbmRleC13aXRoLWFsaWFzJzogaW5kZXhTdW1tYXJ5LndpdGhBbGlhc31cIj5cbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tYXV0b1wiPlxuICAgICAgICA8aSBjbGFzcz1cImZhcyBmYS10YWdcIiAqbmdJZj1cImluZGV4U3VtbWFyeS53aXRoQWxpYXNcIj48L2k+ICZuYnNwO1xuICAgICAgICA8c3Bhbj5WZXJzaW9uIHt7aW5kZXhTdW1tYXJ5LnZlcnNpb259fTwvc3Bhbj5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS1hdXRvXCI+XG4gICAgICAgIDxzcGFuPnt7aW5kZXhTdW1tYXJ5LmRvY0NvdW50fX0gZG9jdW1lbnRzPC9zcGFuPlxuICAgICAgPC9kaXY+XG4gICAgICA8YnV0dG9uIG1hdC1idXR0b24gY29sb3I9XCJ3YXJuXCIgY2xhc3M9XCJjb2wtc20tYXV0b1wiXG4gICAgICAgICAgICAgIHRpdGxlPVwiRGVsZXRlIGluZGV4XCJcbiAgICAgICAgICAgICAgKGNsaWNrKT1cImVzQWRtaW5TZXJ2aWNlLmRlbGV0ZUluZGV4KGluZGV4U3VtbWFyeS5uYW1lKVwiPlxuICAgICAgICA8aSBjbGFzcz1cImZhcyBmYS10cmFzaFwiPjwvaT5cbiAgICAgIDwvYnV0dG9uPlxuICAgICAgPGJ1dHRvbiBtYXQtYnV0dG9uIGNvbG9yPVwicHJpbWFyeVwiXG4gICAgICAgICAgICAgIGNsYXNzPVwiY29sLXNtLWF1dG9cIlxuICAgICAgICAgICAgICAqbmdJZj1cIiFpbmRleFN1bW1hcnkud2l0aEFsaWFzXCJcbiAgICAgICAgICAgICAgdGl0bGU9XCJTd2l0Y2ggQWxpYXNcIlxuICAgICAgICAgICAgICAoY2xpY2spPVwiZXNBZG1pblNlcnZpY2Uuc3dpdGNoQWxpYXMoaW5kZXhTdW1tYXJ5Lm5hbWUpXCI+XG4gICAgICAgIDxpIGNsYXNzPVwiZmFzIGZhLXRhZ1wiPjwvaT5cbiAgICAgIDwvYnV0dG9uPlxuICAgIDwvbWF0LWxpc3QtaXRlbT5cbiAgPC9tYXQtbGlzdD5cblxuICA8ZGl2IGNsYXNzPVwiZXMtZW50aXR5LWFkbWluLWJ1dHRvbi1yb3dcIj5cbiAgICA8YnV0dG9uIG1hdC1yYWlzZWQtYnV0dG9uIGNvbG9yPVwicmFpc2VkLWluZm9cIiAoY2xpY2spPVwiZXNBZG1pblNlcnZpY2Uuc2V0dXBUZW1wbGF0ZSgpXCI+XG4gICAgICA8aSBjbGFzcz1cImZhcyBmYS1ib29rXCI+PC9pPiAmbmJzcDsgU2V0dXAgVGVtcGxhdGVcbiAgICA8L2J1dHRvbj5cbiAgICA8YnV0dG9uIG1hdC1yYWlzZWQtYnV0dG9uIGNvbG9yPVwicmFpc2VkLXdhcm5pbmdcIiAoY2xpY2spPVwiZXNBZG1pblNlcnZpY2UudXBkYXRlVmVyc2lvbigpXCI+XG4gICAgICA8aSBjbGFzcz1cImZhcyBmYS1hcnJvdy11cFwiPjwvaT4gJm5ic3A7IFVwZGF0ZSBWZXJzaW9uXG4gICAgPC9idXR0b24+XG4gIDwvZGl2PlxuXG48L2Rpdj5cbmAsXG4gIHN0eWxlczogW2AuZXMtZW50aXR5LWFkbWluLW1lbnUtY29udGFpbmVye21hcmdpbi10b3A6NDBweH0uZXMtZW50aXR5LWFkbWluLW1lbnUtY29udGFpbmVyIGgye2NvbG9yOiMwMDA7Ym9yZGVyLWJvdHRvbTpub25lfS5lcy1lbnRpdHktYWRtaW4tbWVudS1jb250YWluZXIgLmluZGV4LXdpdGgtYWxpYXN7Zm9udC13ZWlnaHQ6NzAwO2JvcmRlci1ib3R0b206bm9uZX0uZXMtZW50aXR5LWFkbWluLWJ1dHRvbi1yb3d7dGV4dC1hbGlnbjpjZW50ZXJ9LmVzLWVudGl0eS1hZG1pbi1idXR0b24tcm93IGJ1dHRvbnttYXJnaW4tcmlnaHQ6NXB4fWBdXG59KVxuZXhwb3J0IGNsYXNzIEVzRW50aXR5QWRtaW5Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGVzQWRtaW5TZXJ2aWNlOiBFc0FkbWluU2VydmljZTtcbiAgQElucHV0KCkgdGl0bGU6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuZXNBZG1pblNlcnZpY2UucmVmcmVzaFN1bW1hcnkoKTtcbiAgfVxufVxuIl19
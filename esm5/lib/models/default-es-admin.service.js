/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { HttpParams } from '@angular/common/http';
var DefaultEsAdminService = /** @class */ (function () {
    function DefaultEsAdminService(_http, baseURl, entityName) {
        this.http = _http;
        this.BASE_URL = baseURl;
        this.ENTITY_NAME = entityName;
    }
    /**
     * @return {?}
     */
    DefaultEsAdminService.prototype.refreshSummary = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.http.get(this.BASE_URL + 'indices-summary', {}).subscribe(function (response) { return _this.summary = response.sort(function (a, b) {
            return a.version - b.version;
        }); }, function () { return console.error('Unable to retrieve index summary for ' + _this.ENTITY_NAME); });
    };
    /**
     * @return {?}
     */
    DefaultEsAdminService.prototype.setupTemplate = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.http.post(this.BASE_URL + 'setup-index-template', {}, {}).subscribe(function () {
            _this.refreshSummary();
            console.info('Mapping template successfully setup for ' + _this.ENTITY_NAME);
        }, function () {
            console.error('Unable to setup mapping template for ' + _this.ENTITY_NAME);
        });
    };
    /**
     * @return {?}
     */
    DefaultEsAdminService.prototype.updateVersion = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.http.post(this.BASE_URL + 'update-index-version', {}, {}).subscribe(function () {
            console.info('Updated version of ' + _this.ENTITY_NAME + ' index');
            setTimeout(function () { return _this.refreshSummary(); }, 500);
        }, function () {
            console.error('Unable to update index version for ' + _this.ENTITY_NAME);
        });
    };
    /**
     * @param {?} target
     * @return {?}
     */
    DefaultEsAdminService.prototype.switchAlias = /**
     * @param {?} target
     * @return {?}
     */
    function (target) {
        var _this = this;
        this.http.post(this.BASE_URL + 'switch-index-alias', {}, {
            params: new HttpParams().append('esIndexName', target)
        }).subscribe(function () { return _this.refreshSummary(); }, function () {
            console.error('Unable to switch alias to index ' + target);
        });
    };
    /**
     * @param {?} target
     * @return {?}
     */
    DefaultEsAdminService.prototype.deleteIndex = /**
     * @param {?} target
     * @return {?}
     */
    function (target) {
        var _this = this;
        this.http.post(this.BASE_URL + 'delete-index', {}, {
            params: new HttpParams().append('esIndexName', target)
        }).subscribe(function () { return _this.refreshSummary(); }, function () {
            console.error('Unable to delete index ' + target);
        });
    };
    return DefaultEsAdminService;
}());
export { DefaultEsAdminService };
if (false) {
    /** @type {?} */
    DefaultEsAdminService.prototype.http;
    /** @type {?} */
    DefaultEsAdminService.prototype.BASE_URL;
    /** @type {?} */
    DefaultEsAdminService.prototype.ENTITY_NAME;
    /** @type {?} */
    DefaultEsAdminService.prototype.summary;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVmYXVsdC1lcy1hZG1pbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2RlZmF1bHQtZXMtYWRtaW4uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFhLFVBQVUsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBSzVELElBQUE7SUFPRSwrQkFBWSxLQUFpQixFQUFFLE9BQWUsRUFBRSxVQUFrQjtRQUNoRSxJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztRQUNsQixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztRQUN4QixJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQztLQUMvQjs7OztJQUdNLDhDQUFjOzs7OztRQUNuQixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FDWCxJQUFJLENBQUMsUUFBUSxHQUFHLGlCQUFpQixFQUNqQyxFQUFFLENBQ0gsQ0FBQyxTQUFTLENBQ1QsVUFBQSxRQUFRLElBQUksT0FBQSxLQUFJLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQztZQUNyRCxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDO1NBQzlCLENBQUMsRUFGVSxDQUVWLEVBQ0YsY0FBTSxPQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsdUNBQXVDLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxFQUF6RSxDQUF5RSxDQUNoRixDQUFDOzs7OztJQUdHLDZDQUFhOzs7OztRQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDWixJQUFJLENBQUMsUUFBUSxHQUFHLHNCQUFzQixFQUN0QyxFQUFFLEVBQ0YsRUFBRSxDQUNILENBQUMsU0FBUyxDQUNUO1lBQ0UsS0FBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3RCLE9BQU8sQ0FBQyxJQUFJLENBQUMsMENBQTBDLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQzdFLEVBQ0Q7WUFDRSxPQUFPLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztTQUMzRSxDQUNGLENBQUM7Ozs7O0lBR0csNkNBQWE7Ozs7O1FBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUNaLElBQUksQ0FBQyxRQUFRLEdBQUcsc0JBQXNCLEVBQ3RDLEVBQUUsRUFDRixFQUFFLENBQ0gsQ0FBQyxTQUFTLENBQ1Q7WUFDRSxPQUFPLENBQUMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxDQUFDLENBQUM7WUFDbEUsVUFBVSxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsY0FBYyxFQUFFLEVBQXJCLENBQXFCLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDOUMsRUFDRDtZQUNFLE9BQU8sQ0FBQyxLQUFLLENBQUMscUNBQXFDLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ3pFLENBQ0YsQ0FBQzs7Ozs7O0lBR0csMkNBQVc7Ozs7Y0FBQyxNQUFjOztRQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDWixJQUFJLENBQUMsUUFBUSxHQUFHLG9CQUFvQixFQUNwQyxFQUFFLEVBQ0Y7WUFDRSxNQUFNLEVBQUUsSUFBSSxVQUFVLEVBQUUsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQztTQUN2RCxDQUNGLENBQUMsU0FBUyxDQUNULGNBQU0sT0FBQSxLQUFJLENBQUMsY0FBYyxFQUFFLEVBQXJCLENBQXFCLEVBQzNCO1lBQ0UsT0FBTyxDQUFDLEtBQUssQ0FBQyxrQ0FBa0MsR0FBRyxNQUFNLENBQUMsQ0FBQztTQUM1RCxDQUNGLENBQUM7Ozs7OztJQUdHLDJDQUFXOzs7O2NBQUMsTUFBYzs7UUFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQ1osSUFBSSxDQUFDLFFBQVEsR0FBRyxjQUFjLEVBQzlCLEVBQUUsRUFDRjtZQUNFLE1BQU0sRUFBRSxJQUFJLFVBQVUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsTUFBTSxDQUFDO1NBQ3ZELENBQ0YsQ0FBQyxTQUFTLENBQ1QsY0FBTSxPQUFBLEtBQUksQ0FBQyxjQUFjLEVBQUUsRUFBckIsQ0FBcUIsRUFDM0I7WUFDRSxPQUFPLENBQUMsS0FBSyxDQUFDLHlCQUF5QixHQUFHLE1BQU0sQ0FBQyxDQUFDO1NBQ25ELENBQ0YsQ0FBQzs7Z0NBMUZOO0lBNkZDLENBQUE7QUF4RkQsaUNBd0ZDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtIdHRwQ2xpZW50LCBIdHRwUGFyYW1zfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQge0VzQWRtaW5TZXJ2aWNlfSBmcm9tICcuL2VzLWFkbWluLXNlcnZpY2UnO1xuaW1wb3J0IHtFc0luZGV4U3VtbWFyeX0gZnJvbSAnLi9lcy1pbmRleC1zdW1tYXJ5JztcblxuXG5leHBvcnQgY2xhc3MgRGVmYXVsdEVzQWRtaW5TZXJ2aWNlIGltcGxlbWVudHMgRXNBZG1pblNlcnZpY2Uge1xuXG4gIHByaXZhdGUgaHR0cDogSHR0cENsaWVudDtcbiAgQkFTRV9VUkw6IHN0cmluZztcbiAgRU5USVRZX05BTUU6IHN0cmluZztcbiAgc3VtbWFyeTogRXNJbmRleFN1bW1hcnlbXTtcblxuICBjb25zdHJ1Y3RvcihfaHR0cDogSHR0cENsaWVudCwgYmFzZVVSbDogc3RyaW5nLCBlbnRpdHlOYW1lOiBzdHJpbmcpIHtcbiAgICB0aGlzLmh0dHAgPSBfaHR0cDtcbiAgICB0aGlzLkJBU0VfVVJMID0gYmFzZVVSbDtcbiAgICB0aGlzLkVOVElUWV9OQU1FID0gZW50aXR5TmFtZTtcbiAgfVxuXG5cbiAgcHVibGljIHJlZnJlc2hTdW1tYXJ5KCk6IHZvaWQge1xuICAgIHRoaXMuaHR0cC5nZXQ8RXNJbmRleFN1bW1hcnlbXT4oXG4gICAgICB0aGlzLkJBU0VfVVJMICsgJ2luZGljZXMtc3VtbWFyeScsXG4gICAgICB7fVxuICAgICkuc3Vic2NyaWJlKFxuICAgICAgcmVzcG9uc2UgPT4gdGhpcy5zdW1tYXJ5ID0gcmVzcG9uc2Uuc29ydChmdW5jdGlvbiAoYSwgYikge1xuICAgICAgICByZXR1cm4gYS52ZXJzaW9uIC0gYi52ZXJzaW9uO1xuICAgICAgfSksXG4gICAgICAoKSA9PiBjb25zb2xlLmVycm9yKCdVbmFibGUgdG8gcmV0cmlldmUgaW5kZXggc3VtbWFyeSBmb3IgJyArIHRoaXMuRU5USVRZX05BTUUpXG4gICAgKTtcbiAgfVxuXG4gIHB1YmxpYyBzZXR1cFRlbXBsYXRlKCk6IHZvaWQge1xuICAgIHRoaXMuaHR0cC5wb3N0KFxuICAgICAgdGhpcy5CQVNFX1VSTCArICdzZXR1cC1pbmRleC10ZW1wbGF0ZScsXG4gICAgICB7fSxcbiAgICAgIHt9XG4gICAgKS5zdWJzY3JpYmUoXG4gICAgICAoKSA9PiB7XG4gICAgICAgIHRoaXMucmVmcmVzaFN1bW1hcnkoKTtcbiAgICAgICAgY29uc29sZS5pbmZvKCdNYXBwaW5nIHRlbXBsYXRlIHN1Y2Nlc3NmdWxseSBzZXR1cCBmb3IgJyArIHRoaXMuRU5USVRZX05BTUUpO1xuICAgICAgfSxcbiAgICAgICgpID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcignVW5hYmxlIHRvIHNldHVwIG1hcHBpbmcgdGVtcGxhdGUgZm9yICcgKyB0aGlzLkVOVElUWV9OQU1FKTtcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbiAgcHVibGljIHVwZGF0ZVZlcnNpb24oKTogdm9pZCB7XG4gICAgdGhpcy5odHRwLnBvc3QoXG4gICAgICB0aGlzLkJBU0VfVVJMICsgJ3VwZGF0ZS1pbmRleC12ZXJzaW9uJyxcbiAgICAgIHt9LFxuICAgICAge31cbiAgICApLnN1YnNjcmliZShcbiAgICAgICgpID0+IHtcbiAgICAgICAgY29uc29sZS5pbmZvKCdVcGRhdGVkIHZlcnNpb24gb2YgJyArIHRoaXMuRU5USVRZX05BTUUgKyAnIGluZGV4Jyk7XG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4gdGhpcy5yZWZyZXNoU3VtbWFyeSgpLCA1MDApO1xuICAgICAgfSxcbiAgICAgICgpID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcignVW5hYmxlIHRvIHVwZGF0ZSBpbmRleCB2ZXJzaW9uIGZvciAnICsgdGhpcy5FTlRJVFlfTkFNRSk7XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIHB1YmxpYyBzd2l0Y2hBbGlhcyh0YXJnZXQ6IHN0cmluZykge1xuICAgIHRoaXMuaHR0cC5wb3N0KFxuICAgICAgdGhpcy5CQVNFX1VSTCArICdzd2l0Y2gtaW5kZXgtYWxpYXMnLFxuICAgICAge30sXG4gICAgICB7XG4gICAgICAgIHBhcmFtczogbmV3IEh0dHBQYXJhbXMoKS5hcHBlbmQoJ2VzSW5kZXhOYW1lJywgdGFyZ2V0KVxuICAgICAgfVxuICAgICkuc3Vic2NyaWJlKFxuICAgICAgKCkgPT4gdGhpcy5yZWZyZXNoU3VtbWFyeSgpLFxuICAgICAgKCkgPT4ge1xuICAgICAgICBjb25zb2xlLmVycm9yKCdVbmFibGUgdG8gc3dpdGNoIGFsaWFzIHRvIGluZGV4ICcgKyB0YXJnZXQpO1xuICAgICAgfVxuICAgICk7XG4gIH1cblxuICBwdWJsaWMgZGVsZXRlSW5kZXgodGFyZ2V0OiBzdHJpbmcpIHtcbiAgICB0aGlzLmh0dHAucG9zdChcbiAgICAgIHRoaXMuQkFTRV9VUkwgKyAnZGVsZXRlLWluZGV4JyxcbiAgICAgIHt9LFxuICAgICAge1xuICAgICAgICBwYXJhbXM6IG5ldyBIdHRwUGFyYW1zKCkuYXBwZW5kKCdlc0luZGV4TmFtZScsIHRhcmdldClcbiAgICAgIH1cbiAgICApLnN1YnNjcmliZShcbiAgICAgICgpID0+IHRoaXMucmVmcmVzaFN1bW1hcnkoKSxcbiAgICAgICgpID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcignVW5hYmxlIHRvIGRlbGV0ZSBpbmRleCAnICsgdGFyZ2V0KTtcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbn1cbiJdfQ==
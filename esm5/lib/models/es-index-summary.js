/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function EsIndexSummary() { }
/** @type {?} */
EsIndexSummary.prototype.name;
/** @type {?} */
EsIndexSummary.prototype.version;
/** @type {?} */
EsIndexSummary.prototype.withAlias;
/** @type {?} */
EsIndexSummary.prototype.docCount;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXMtaW5kZXgtc3VtbWFyeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1lbGFzdGljc2VhcmNoLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9lcy1pbmRleC1zdW1tYXJ5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIEVzSW5kZXhTdW1tYXJ5IHtcbiAgbmFtZTogc3RyaW5nO1xuICB2ZXJzaW9uOiBudW1iZXI7XG4gIHdpdGhBbGlhczogYm9vbGVhbjtcbiAgZG9jQ291bnQ6IG51bWJlcjtcbn1cbiJdfQ==
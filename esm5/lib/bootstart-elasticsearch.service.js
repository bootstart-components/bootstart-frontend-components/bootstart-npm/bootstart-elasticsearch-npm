/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var BootstartElasticsearchService = /** @class */ (function () {
    function BootstartElasticsearchService() {
    }
    BootstartElasticsearchService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
    ];
    /** @nocollapse */
    BootstartElasticsearchService.ctorParameters = function () { return []; };
    /** @nocollapse */ BootstartElasticsearchService.ngInjectableDef = i0.defineInjectable({ factory: function BootstartElasticsearchService_Factory() { return new BootstartElasticsearchService(); }, token: BootstartElasticsearchService, providedIn: "root" });
    return BootstartElasticsearchService;
}());
export { BootstartElasticsearchService };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2guc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1lbGFzdGljc2VhcmNoLyIsInNvdXJjZXMiOlsibGliL2Jvb3RzdGFydC1lbGFzdGljc2VhcmNoLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7OztJQU96QztLQUFpQjs7Z0JBTGxCLFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7O3dDQUpEOztTQUthLDZCQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0RWxhc3RpY3NlYXJjaFNlcnZpY2Uge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG59XG4iXX0=
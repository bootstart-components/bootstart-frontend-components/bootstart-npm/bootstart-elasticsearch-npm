/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { EsEntityAdminComponent } from './components/es-entity-admin/es-entity-admin.component';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatListModule } from '@angular/material';
var BootstartElasticsearchModule = /** @class */ (function () {
    function BootstartElasticsearchModule() {
    }
    BootstartElasticsearchModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        MatListModule,
                        MatButtonModule
                    ],
                    declarations: [
                        EsEntityAdminComponent
                    ],
                    exports: [
                        EsEntityAdminComponent
                    ]
                },] },
    ];
    return BootstartElasticsearchModule;
}());
export { BootstartElasticsearchModule };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gvIiwic291cmNlcyI6WyJsaWIvYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLHdEQUF3RCxDQUFDO0FBQzlGLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsZUFBZSxFQUFFLGFBQWEsRUFBQyxNQUFNLG1CQUFtQixDQUFDOzs7OztnQkFFaEUsUUFBUSxTQUFDO29CQUNFLE9BQU8sRUFBTzt3QkFDWixZQUFZO3dCQUNaLGFBQWE7d0JBQ2IsZUFBZTtxQkFDaEI7b0JBQ0QsWUFBWSxFQUFFO3dCQUNaLHNCQUFzQjtxQkFDdkI7b0JBQ0QsT0FBTyxFQUFPO3dCQUNaLHNCQUFzQjtxQkFDdkI7aUJBQ0Y7O3VDQWpCWDs7U0FrQmEsNEJBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0VzRW50aXR5QWRtaW5Db21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9lcy1lbnRpdHktYWRtaW4vZXMtZW50aXR5LWFkbWluLmNvbXBvbmVudCc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7TWF0QnV0dG9uTW9kdWxlLCBNYXRMaXN0TW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5cbkBOZ01vZHVsZSh7XG4gICAgICAgICAgICBpbXBvcnRzICAgICA6IFtcbiAgICAgICAgICAgICAgQ29tbW9uTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRMaXN0TW9kdWxlLFxuICAgICAgICAgICAgICBNYXRCdXR0b25Nb2R1bGVcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgICAgICAgRXNFbnRpdHlBZG1pbkNvbXBvbmVudFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGV4cG9ydHMgICAgIDogW1xuICAgICAgICAgICAgICBFc0VudGl0eUFkbWluQ29tcG9uZW50XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRFbGFzdGljc2VhcmNoTW9kdWxlIHtcbn1cbiJdfQ==
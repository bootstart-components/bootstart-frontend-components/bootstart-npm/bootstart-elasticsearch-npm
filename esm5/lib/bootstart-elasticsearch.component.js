/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var BootstartElasticsearchComponent = /** @class */ (function () {
    function BootstartElasticsearchComponent() {
    }
    /**
     * @return {?}
     */
    BootstartElasticsearchComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    BootstartElasticsearchComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-bootstart-elasticsearch',
                    template: "\n    <p>\n      bootstart-elasticsearch works!\n    </p>\n  ",
                    styles: []
                },] },
    ];
    /** @nocollapse */
    BootstartElasticsearchComponent.ctorParameters = function () { return []; };
    return BootstartElasticsearchComponent;
}());
export { BootstartElasticsearchComponent };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2guY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gvIiwic291cmNlcyI6WyJsaWIvYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2guY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDOztJQWFoRDtLQUFpQjs7OztJQUVqQixrREFBUTs7O0lBQVI7S0FDQzs7Z0JBZEYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSw2QkFBNkI7b0JBQ3ZDLFFBQVEsRUFBRSwrREFJVDtvQkFDRCxNQUFNLEVBQUUsRUFBRTtpQkFDWDs7OzswQ0FWRDs7U0FXYSwrQkFBK0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsaWItYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxwPlxuICAgICAgYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2ggd29ya3MhXG4gICAgPC9wPlxuICBgLFxuICBzdHlsZXM6IFtdXG59KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEVsYXN0aWNzZWFyY2hDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxufVxuIl19
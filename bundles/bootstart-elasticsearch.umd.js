(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('@angular/material'), require('@angular/common/http')) :
    typeof define === 'function' && define.amd ? define('bootstart-elasticsearch', ['exports', '@angular/core', '@angular/common', '@angular/material', '@angular/common/http'], factory) :
    (factory((global['bootstart-elasticsearch'] = {}),global.ng.core,global.ng.common,global.ng.material,global.ng.common.http));
}(this, (function (exports,core,common,material,http) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var EsEntityAdminComponent = (function () {
        function EsEntityAdminComponent() {
        }
        /**
         * @return {?}
         */
        EsEntityAdminComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this.esAdminService.refreshSummary();
            };
        EsEntityAdminComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'bootstart-es-entity-admin',
                        template: "<div class=\"es-entity-admin-menu-container\">\n  <h2>{{title}}</h2>\n\n  <mat-list>\n    <mat-list-item *ngFor=\"let indexSummary of esAdminService.summary\" class=\"row\"\n                   [ngClass]=\"{'index-with-alias': indexSummary.withAlias}\">\n      <div class=\"col-sm-auto\">\n        <i class=\"fas fa-tag\" *ngIf=\"indexSummary.withAlias\"></i> &nbsp;\n        <span>Version {{indexSummary.version}}</span>\n      </div>\n      <div class=\"col-sm-auto\">\n        <span>{{indexSummary.docCount}} documents</span>\n      </div>\n      <button mat-button color=\"warn\" class=\"col-sm-auto\"\n              title=\"Delete index\"\n              (click)=\"esAdminService.deleteIndex(indexSummary.name)\">\n        <i class=\"fas fa-trash\"></i>\n      </button>\n      <button mat-button color=\"primary\"\n              class=\"col-sm-auto\"\n              *ngIf=\"!indexSummary.withAlias\"\n              title=\"Switch Alias\"\n              (click)=\"esAdminService.switchAlias(indexSummary.name)\">\n        <i class=\"fas fa-tag\"></i>\n      </button>\n    </mat-list-item>\n  </mat-list>\n\n  <div class=\"es-entity-admin-button-row\">\n    <button mat-raised-button color=\"raised-info\" (click)=\"esAdminService.setupTemplate()\">\n      <i class=\"fas fa-book\"></i> &nbsp; Setup Template\n    </button>\n    <button mat-raised-button color=\"raised-warning\" (click)=\"esAdminService.updateVersion()\">\n      <i class=\"fas fa-arrow-up\"></i> &nbsp; Update Version\n    </button>\n  </div>\n\n</div>\n",
                        styles: [".es-entity-admin-menu-container{margin-top:40px}.es-entity-admin-menu-container h2{color:#000;border-bottom:none}.es-entity-admin-menu-container .index-with-alias{font-weight:700;border-bottom:none}.es-entity-admin-button-row{text-align:center}.es-entity-admin-button-row button{margin-right:5px}"]
                    },] },
        ];
        /** @nocollapse */
        EsEntityAdminComponent.ctorParameters = function () { return []; };
        EsEntityAdminComponent.propDecorators = {
            esAdminService: [{ type: core.Input }],
            title: [{ type: core.Input }]
        };
        return EsEntityAdminComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var BootstartElasticsearchModule = (function () {
        function BootstartElasticsearchModule() {
        }
        BootstartElasticsearchModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            common.CommonModule,
                            material.MatListModule,
                            material.MatButtonModule
                        ],
                        declarations: [
                            EsEntityAdminComponent
                        ],
                        exports: [
                            EsEntityAdminComponent
                        ]
                    },] },
        ];
        return BootstartElasticsearchModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var DefaultEsAdminService = (function () {
        function DefaultEsAdminService(_http, baseURl, entityName) {
            this.http = _http;
            this.BASE_URL = baseURl;
            this.ENTITY_NAME = entityName;
        }
        /**
         * @return {?}
         */
        DefaultEsAdminService.prototype.refreshSummary = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.http.get(this.BASE_URL + 'indices-summary', {}).subscribe(function (response) {
                    return _this.summary = response.sort(function (a, b) {
                        return a.version - b.version;
                    });
                }, function () { return console.error('Unable to retrieve index summary for ' + _this.ENTITY_NAME); });
            };
        /**
         * @return {?}
         */
        DefaultEsAdminService.prototype.setupTemplate = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.http.post(this.BASE_URL + 'setup-index-template', {}, {}).subscribe(function () {
                    _this.refreshSummary();
                    console.info('Mapping template successfully setup for ' + _this.ENTITY_NAME);
                }, function () {
                    console.error('Unable to setup mapping template for ' + _this.ENTITY_NAME);
                });
            };
        /**
         * @return {?}
         */
        DefaultEsAdminService.prototype.updateVersion = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.http.post(this.BASE_URL + 'update-index-version', {}, {}).subscribe(function () {
                    console.info('Updated version of ' + _this.ENTITY_NAME + ' index');
                    setTimeout(function () { return _this.refreshSummary(); }, 500);
                }, function () {
                    console.error('Unable to update index version for ' + _this.ENTITY_NAME);
                });
            };
        /**
         * @param {?} target
         * @return {?}
         */
        DefaultEsAdminService.prototype.switchAlias = /**
         * @param {?} target
         * @return {?}
         */
            function (target) {
                var _this = this;
                this.http.post(this.BASE_URL + 'switch-index-alias', {}, {
                    params: new http.HttpParams().append('esIndexName', target)
                }).subscribe(function () { return _this.refreshSummary(); }, function () {
                    console.error('Unable to switch alias to index ' + target);
                });
            };
        /**
         * @param {?} target
         * @return {?}
         */
        DefaultEsAdminService.prototype.deleteIndex = /**
         * @param {?} target
         * @return {?}
         */
            function (target) {
                var _this = this;
                this.http.post(this.BASE_URL + 'delete-index', {}, {
                    params: new http.HttpParams().append('esIndexName', target)
                }).subscribe(function () { return _this.refreshSummary(); }, function () {
                    console.error('Unable to delete index ' + target);
                });
            };
        return DefaultEsAdminService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    exports.BootstartElasticsearchModule = BootstartElasticsearchModule;
    exports.DefaultEsAdminService = DefaultEsAdminService;
    exports.ɵa = EsEntityAdminComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gudW1kLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9ib290c3RhcnQtZWxhc3RpY3NlYXJjaC9saWIvY29tcG9uZW50cy9lcy1lbnRpdHktYWRtaW4vZXMtZW50aXR5LWFkbWluLmNvbXBvbmVudC50cyIsIm5nOi8vYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gvbGliL2Jvb3RzdGFydC1lbGFzdGljc2VhcmNoLm1vZHVsZS50cyIsIm5nOi8vYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gvbGliL21vZGVscy9kZWZhdWx0LWVzLWFkbWluLnNlcnZpY2UudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtFc0FkbWluU2VydmljZX0gZnJvbSAnLi4vLi4vbW9kZWxzL2VzLWFkbWluLXNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdib290c3RhcnQtZXMtZW50aXR5LWFkbWluJyxcbiAgdGVtcGxhdGU6IGA8ZGl2IGNsYXNzPVwiZXMtZW50aXR5LWFkbWluLW1lbnUtY29udGFpbmVyXCI+XG4gIDxoMj57e3RpdGxlfX08L2gyPlxuXG4gIDxtYXQtbGlzdD5cbiAgICA8bWF0LWxpc3QtaXRlbSAqbmdGb3I9XCJsZXQgaW5kZXhTdW1tYXJ5IG9mIGVzQWRtaW5TZXJ2aWNlLnN1bW1hcnlcIiBjbGFzcz1cInJvd1wiXG4gICAgICAgICAgICAgICAgICAgW25nQ2xhc3NdPVwieydpbmRleC13aXRoLWFsaWFzJzogaW5kZXhTdW1tYXJ5LndpdGhBbGlhc31cIj5cbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tYXV0b1wiPlxuICAgICAgICA8aSBjbGFzcz1cImZhcyBmYS10YWdcIiAqbmdJZj1cImluZGV4U3VtbWFyeS53aXRoQWxpYXNcIj48L2k+ICZuYnNwO1xuICAgICAgICA8c3Bhbj5WZXJzaW9uIHt7aW5kZXhTdW1tYXJ5LnZlcnNpb259fTwvc3Bhbj5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS1hdXRvXCI+XG4gICAgICAgIDxzcGFuPnt7aW5kZXhTdW1tYXJ5LmRvY0NvdW50fX0gZG9jdW1lbnRzPC9zcGFuPlxuICAgICAgPC9kaXY+XG4gICAgICA8YnV0dG9uIG1hdC1idXR0b24gY29sb3I9XCJ3YXJuXCIgY2xhc3M9XCJjb2wtc20tYXV0b1wiXG4gICAgICAgICAgICAgIHRpdGxlPVwiRGVsZXRlIGluZGV4XCJcbiAgICAgICAgICAgICAgKGNsaWNrKT1cImVzQWRtaW5TZXJ2aWNlLmRlbGV0ZUluZGV4KGluZGV4U3VtbWFyeS5uYW1lKVwiPlxuICAgICAgICA8aSBjbGFzcz1cImZhcyBmYS10cmFzaFwiPjwvaT5cbiAgICAgIDwvYnV0dG9uPlxuICAgICAgPGJ1dHRvbiBtYXQtYnV0dG9uIGNvbG9yPVwicHJpbWFyeVwiXG4gICAgICAgICAgICAgIGNsYXNzPVwiY29sLXNtLWF1dG9cIlxuICAgICAgICAgICAgICAqbmdJZj1cIiFpbmRleFN1bW1hcnkud2l0aEFsaWFzXCJcbiAgICAgICAgICAgICAgdGl0bGU9XCJTd2l0Y2ggQWxpYXNcIlxuICAgICAgICAgICAgICAoY2xpY2spPVwiZXNBZG1pblNlcnZpY2Uuc3dpdGNoQWxpYXMoaW5kZXhTdW1tYXJ5Lm5hbWUpXCI+XG4gICAgICAgIDxpIGNsYXNzPVwiZmFzIGZhLXRhZ1wiPjwvaT5cbiAgICAgIDwvYnV0dG9uPlxuICAgIDwvbWF0LWxpc3QtaXRlbT5cbiAgPC9tYXQtbGlzdD5cblxuICA8ZGl2IGNsYXNzPVwiZXMtZW50aXR5LWFkbWluLWJ1dHRvbi1yb3dcIj5cbiAgICA8YnV0dG9uIG1hdC1yYWlzZWQtYnV0dG9uIGNvbG9yPVwicmFpc2VkLWluZm9cIiAoY2xpY2spPVwiZXNBZG1pblNlcnZpY2Uuc2V0dXBUZW1wbGF0ZSgpXCI+XG4gICAgICA8aSBjbGFzcz1cImZhcyBmYS1ib29rXCI+PC9pPiAmbmJzcDsgU2V0dXAgVGVtcGxhdGVcbiAgICA8L2J1dHRvbj5cbiAgICA8YnV0dG9uIG1hdC1yYWlzZWQtYnV0dG9uIGNvbG9yPVwicmFpc2VkLXdhcm5pbmdcIiAoY2xpY2spPVwiZXNBZG1pblNlcnZpY2UudXBkYXRlVmVyc2lvbigpXCI+XG4gICAgICA8aSBjbGFzcz1cImZhcyBmYS1hcnJvdy11cFwiPjwvaT4gJm5ic3A7IFVwZGF0ZSBWZXJzaW9uXG4gICAgPC9idXR0b24+XG4gIDwvZGl2PlxuXG48L2Rpdj5cbmAsXG4gIHN0eWxlczogW2AuZXMtZW50aXR5LWFkbWluLW1lbnUtY29udGFpbmVye21hcmdpbi10b3A6NDBweH0uZXMtZW50aXR5LWFkbWluLW1lbnUtY29udGFpbmVyIGgye2NvbG9yOiMwMDA7Ym9yZGVyLWJvdHRvbTpub25lfS5lcy1lbnRpdHktYWRtaW4tbWVudS1jb250YWluZXIgLmluZGV4LXdpdGgtYWxpYXN7Zm9udC13ZWlnaHQ6NzAwO2JvcmRlci1ib3R0b206bm9uZX0uZXMtZW50aXR5LWFkbWluLWJ1dHRvbi1yb3d7dGV4dC1hbGlnbjpjZW50ZXJ9LmVzLWVudGl0eS1hZG1pbi1idXR0b24tcm93IGJ1dHRvbnttYXJnaW4tcmlnaHQ6NXB4fWBdXG59KVxuZXhwb3J0IGNsYXNzIEVzRW50aXR5QWRtaW5Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGVzQWRtaW5TZXJ2aWNlOiBFc0FkbWluU2VydmljZTtcbiAgQElucHV0KCkgdGl0bGU6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuZXNBZG1pblNlcnZpY2UucmVmcmVzaFN1bW1hcnkoKTtcbiAgfVxufVxuIiwiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0VzRW50aXR5QWRtaW5Db21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9lcy1lbnRpdHktYWRtaW4vZXMtZW50aXR5LWFkbWluLmNvbXBvbmVudCc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7TWF0QnV0dG9uTW9kdWxlLCBNYXRMaXN0TW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5cbkBOZ01vZHVsZSh7XG4gICAgICAgICAgICBpbXBvcnRzICAgICA6IFtcbiAgICAgICAgICAgICAgQ29tbW9uTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRMaXN0TW9kdWxlLFxuICAgICAgICAgICAgICBNYXRCdXR0b25Nb2R1bGVcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgICAgICAgRXNFbnRpdHlBZG1pbkNvbXBvbmVudFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGV4cG9ydHMgICAgIDogW1xuICAgICAgICAgICAgICBFc0VudGl0eUFkbWluQ29tcG9uZW50XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRFbGFzdGljc2VhcmNoTW9kdWxlIHtcbn1cbiIsImltcG9ydCB7SHR0cENsaWVudCwgSHR0cFBhcmFtc30gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHtFc0FkbWluU2VydmljZX0gZnJvbSAnLi9lcy1hZG1pbi1zZXJ2aWNlJztcbmltcG9ydCB7RXNJbmRleFN1bW1hcnl9IGZyb20gJy4vZXMtaW5kZXgtc3VtbWFyeSc7XG5cblxuZXhwb3J0IGNsYXNzIERlZmF1bHRFc0FkbWluU2VydmljZSBpbXBsZW1lbnRzIEVzQWRtaW5TZXJ2aWNlIHtcblxuICBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQ7XG4gIEJBU0VfVVJMOiBzdHJpbmc7XG4gIEVOVElUWV9OQU1FOiBzdHJpbmc7XG4gIHN1bW1hcnk6IEVzSW5kZXhTdW1tYXJ5W107XG5cbiAgY29uc3RydWN0b3IoX2h0dHA6IEh0dHBDbGllbnQsIGJhc2VVUmw6IHN0cmluZywgZW50aXR5TmFtZTogc3RyaW5nKSB7XG4gICAgdGhpcy5odHRwID0gX2h0dHA7XG4gICAgdGhpcy5CQVNFX1VSTCA9IGJhc2VVUmw7XG4gICAgdGhpcy5FTlRJVFlfTkFNRSA9IGVudGl0eU5hbWU7XG4gIH1cblxuXG4gIHB1YmxpYyByZWZyZXNoU3VtbWFyeSgpOiB2b2lkIHtcbiAgICB0aGlzLmh0dHAuZ2V0PEVzSW5kZXhTdW1tYXJ5W10+KFxuICAgICAgdGhpcy5CQVNFX1VSTCArICdpbmRpY2VzLXN1bW1hcnknLFxuICAgICAge31cbiAgICApLnN1YnNjcmliZShcbiAgICAgIHJlc3BvbnNlID0+IHRoaXMuc3VtbWFyeSA9IHJlc3BvbnNlLnNvcnQoZnVuY3Rpb24gKGEsIGIpIHtcbiAgICAgICAgcmV0dXJuIGEudmVyc2lvbiAtIGIudmVyc2lvbjtcbiAgICAgIH0pLFxuICAgICAgKCkgPT4gY29uc29sZS5lcnJvcignVW5hYmxlIHRvIHJldHJpZXZlIGluZGV4IHN1bW1hcnkgZm9yICcgKyB0aGlzLkVOVElUWV9OQU1FKVxuICAgICk7XG4gIH1cblxuICBwdWJsaWMgc2V0dXBUZW1wbGF0ZSgpOiB2b2lkIHtcbiAgICB0aGlzLmh0dHAucG9zdChcbiAgICAgIHRoaXMuQkFTRV9VUkwgKyAnc2V0dXAtaW5kZXgtdGVtcGxhdGUnLFxuICAgICAge30sXG4gICAgICB7fVxuICAgICkuc3Vic2NyaWJlKFxuICAgICAgKCkgPT4ge1xuICAgICAgICB0aGlzLnJlZnJlc2hTdW1tYXJ5KCk7XG4gICAgICAgIGNvbnNvbGUuaW5mbygnTWFwcGluZyB0ZW1wbGF0ZSBzdWNjZXNzZnVsbHkgc2V0dXAgZm9yICcgKyB0aGlzLkVOVElUWV9OQU1FKTtcbiAgICAgIH0sXG4gICAgICAoKSA9PiB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ1VuYWJsZSB0byBzZXR1cCBtYXBwaW5nIHRlbXBsYXRlIGZvciAnICsgdGhpcy5FTlRJVFlfTkFNRSk7XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIHB1YmxpYyB1cGRhdGVWZXJzaW9uKCk6IHZvaWQge1xuICAgIHRoaXMuaHR0cC5wb3N0KFxuICAgICAgdGhpcy5CQVNFX1VSTCArICd1cGRhdGUtaW5kZXgtdmVyc2lvbicsXG4gICAgICB7fSxcbiAgICAgIHt9XG4gICAgKS5zdWJzY3JpYmUoXG4gICAgICAoKSA9PiB7XG4gICAgICAgIGNvbnNvbGUuaW5mbygnVXBkYXRlZCB2ZXJzaW9uIG9mICcgKyB0aGlzLkVOVElUWV9OQU1FICsgJyBpbmRleCcpO1xuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHRoaXMucmVmcmVzaFN1bW1hcnkoKSwgNTAwKTtcbiAgICAgIH0sXG4gICAgICAoKSA9PiB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ1VuYWJsZSB0byB1cGRhdGUgaW5kZXggdmVyc2lvbiBmb3IgJyArIHRoaXMuRU5USVRZX05BTUUpO1xuICAgICAgfVxuICAgICk7XG4gIH1cblxuICBwdWJsaWMgc3dpdGNoQWxpYXModGFyZ2V0OiBzdHJpbmcpIHtcbiAgICB0aGlzLmh0dHAucG9zdChcbiAgICAgIHRoaXMuQkFTRV9VUkwgKyAnc3dpdGNoLWluZGV4LWFsaWFzJyxcbiAgICAgIHt9LFxuICAgICAge1xuICAgICAgICBwYXJhbXM6IG5ldyBIdHRwUGFyYW1zKCkuYXBwZW5kKCdlc0luZGV4TmFtZScsIHRhcmdldClcbiAgICAgIH1cbiAgICApLnN1YnNjcmliZShcbiAgICAgICgpID0+IHRoaXMucmVmcmVzaFN1bW1hcnkoKSxcbiAgICAgICgpID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcignVW5hYmxlIHRvIHN3aXRjaCBhbGlhcyB0byBpbmRleCAnICsgdGFyZ2V0KTtcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbiAgcHVibGljIGRlbGV0ZUluZGV4KHRhcmdldDogc3RyaW5nKSB7XG4gICAgdGhpcy5odHRwLnBvc3QoXG4gICAgICB0aGlzLkJBU0VfVVJMICsgJ2RlbGV0ZS1pbmRleCcsXG4gICAgICB7fSxcbiAgICAgIHtcbiAgICAgICAgcGFyYW1zOiBuZXcgSHR0cFBhcmFtcygpLmFwcGVuZCgnZXNJbmRleE5hbWUnLCB0YXJnZXQpXG4gICAgICB9XG4gICAgKS5zdWJzY3JpYmUoXG4gICAgICAoKSA9PiB0aGlzLnJlZnJlc2hTdW1tYXJ5KCksXG4gICAgICAoKSA9PiB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ1VuYWJsZSB0byBkZWxldGUgaW5kZXggJyArIHRhcmdldCk7XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG59XG4iXSwibmFtZXMiOlsiQ29tcG9uZW50IiwiSW5wdXQiLCJOZ01vZHVsZSIsIkNvbW1vbk1vZHVsZSIsIk1hdExpc3RNb2R1bGUiLCJNYXRCdXR0b25Nb2R1bGUiLCJIdHRwUGFyYW1zIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7UUFtREU7U0FDQzs7OztRQUVELHlDQUFROzs7WUFBUjtnQkFDRSxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQ3RDOztvQkFyREZBLGNBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsMkJBQTJCO3dCQUNyQyxRQUFRLEVBQUUsdy9DQXNDWDt3QkFDQyxNQUFNLEVBQUUsQ0FBQywwU0FBMFMsQ0FBQztxQkFDclQ7Ozs7O3FDQUdFQyxVQUFLOzRCQUNMQSxVQUFLOztxQ0FqRFI7Ozs7Ozs7QUNBQTs7OztvQkFLQ0MsYUFBUSxTQUFDO3dCQUNFLE9BQU8sRUFBTzs0QkFDWkMsbUJBQVk7NEJBQ1pDLHNCQUFhOzRCQUNiQyx3QkFBZTt5QkFDaEI7d0JBQ0QsWUFBWSxFQUFFOzRCQUNaLHNCQUFzQjt5QkFDdkI7d0JBQ0QsT0FBTyxFQUFPOzRCQUNaLHNCQUFzQjt5QkFDdkI7cUJBQ0Y7OzJDQWpCWDs7Ozs7OztBQ0FBLFFBS0E7UUFPRSwrQkFBWSxLQUFpQixFQUFFLE9BQWUsRUFBRSxVQUFrQjtZQUNoRSxJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztZQUNsQixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztZQUN4QixJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQztTQUMvQjs7OztRQUdNLDhDQUFjOzs7OztnQkFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQ1gsSUFBSSxDQUFDLFFBQVEsR0FBRyxpQkFBaUIsRUFDakMsRUFBRSxDQUNILENBQUMsU0FBUyxDQUNULFVBQUEsUUFBUTtvQkFBSSxPQUFBLEtBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO3dCQUNyRCxPQUFPLENBQUMsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQztxQkFDOUIsQ0FBQztpQkFBQSxFQUNGLGNBQU0sT0FBQSxPQUFPLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBQSxDQUNoRixDQUFDOzs7OztRQUdHLDZDQUFhOzs7OztnQkFDbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQ1osSUFBSSxDQUFDLFFBQVEsR0FBRyxzQkFBc0IsRUFDdEMsRUFBRSxFQUNGLEVBQUUsQ0FDSCxDQUFDLFNBQVMsQ0FDVDtvQkFDRSxLQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7b0JBQ3RCLE9BQU8sQ0FBQyxJQUFJLENBQUMsMENBQTBDLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2lCQUM3RSxFQUNEO29CQUNFLE9BQU8sQ0FBQyxLQUFLLENBQUMsdUNBQXVDLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2lCQUMzRSxDQUNGLENBQUM7Ozs7O1FBR0csNkNBQWE7Ozs7O2dCQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDWixJQUFJLENBQUMsUUFBUSxHQUFHLHNCQUFzQixFQUN0QyxFQUFFLEVBQ0YsRUFBRSxDQUNILENBQUMsU0FBUyxDQUNUO29CQUNFLE9BQU8sQ0FBQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUMsQ0FBQztvQkFDbEUsVUFBVSxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsY0FBYyxFQUFFLEdBQUEsRUFBRSxHQUFHLENBQUMsQ0FBQztpQkFDOUMsRUFDRDtvQkFDRSxPQUFPLENBQUMsS0FBSyxDQUFDLHFDQUFxQyxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztpQkFDekUsQ0FDRixDQUFDOzs7Ozs7UUFHRywyQ0FBVzs7OztzQkFBQyxNQUFjOztnQkFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQ1osSUFBSSxDQUFDLFFBQVEsR0FBRyxvQkFBb0IsRUFDcEMsRUFBRSxFQUNGO29CQUNFLE1BQU0sRUFBRSxJQUFJQyxlQUFVLEVBQUUsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQztpQkFDdkQsQ0FDRixDQUFDLFNBQVMsQ0FDVCxjQUFNLE9BQUEsS0FBSSxDQUFDLGNBQWMsRUFBRSxHQUFBLEVBQzNCO29CQUNFLE9BQU8sQ0FBQyxLQUFLLENBQUMsa0NBQWtDLEdBQUcsTUFBTSxDQUFDLENBQUM7aUJBQzVELENBQ0YsQ0FBQzs7Ozs7O1FBR0csMkNBQVc7Ozs7c0JBQUMsTUFBYzs7Z0JBQy9CLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUNaLElBQUksQ0FBQyxRQUFRLEdBQUcsY0FBYyxFQUM5QixFQUFFLEVBQ0Y7b0JBQ0UsTUFBTSxFQUFFLElBQUlBLGVBQVUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsTUFBTSxDQUFDO2lCQUN2RCxDQUNGLENBQUMsU0FBUyxDQUNULGNBQU0sT0FBQSxLQUFJLENBQUMsY0FBYyxFQUFFLEdBQUEsRUFDM0I7b0JBQ0UsT0FBTyxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsR0FBRyxNQUFNLENBQUMsQ0FBQztpQkFDbkQsQ0FDRixDQUFDOztvQ0ExRk47UUE2RkM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzsifQ==
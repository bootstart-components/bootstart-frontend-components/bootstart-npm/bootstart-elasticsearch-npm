import { Component, Input, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatListModule } from '@angular/material';
import { HttpParams } from '@angular/common/http';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var EsEntityAdminComponent = /** @class */ (function () {
    function EsEntityAdminComponent() {
    }
    /**
     * @return {?}
     */
    EsEntityAdminComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.esAdminService.refreshSummary();
    };
    EsEntityAdminComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-es-entity-admin',
                    template: "<div class=\"es-entity-admin-menu-container\">\n  <h2>{{title}}</h2>\n\n  <mat-list>\n    <mat-list-item *ngFor=\"let indexSummary of esAdminService.summary\" class=\"row\"\n                   [ngClass]=\"{'index-with-alias': indexSummary.withAlias}\">\n      <div class=\"col-sm-auto\">\n        <i class=\"fas fa-tag\" *ngIf=\"indexSummary.withAlias\"></i> &nbsp;\n        <span>Version {{indexSummary.version}}</span>\n      </div>\n      <div class=\"col-sm-auto\">\n        <span>{{indexSummary.docCount}} documents</span>\n      </div>\n      <button mat-button color=\"warn\" class=\"col-sm-auto\"\n              title=\"Delete index\"\n              (click)=\"esAdminService.deleteIndex(indexSummary.name)\">\n        <i class=\"fas fa-trash\"></i>\n      </button>\n      <button mat-button color=\"primary\"\n              class=\"col-sm-auto\"\n              *ngIf=\"!indexSummary.withAlias\"\n              title=\"Switch Alias\"\n              (click)=\"esAdminService.switchAlias(indexSummary.name)\">\n        <i class=\"fas fa-tag\"></i>\n      </button>\n    </mat-list-item>\n  </mat-list>\n\n  <div class=\"es-entity-admin-button-row\">\n    <button mat-raised-button color=\"raised-info\" (click)=\"esAdminService.setupTemplate()\">\n      <i class=\"fas fa-book\"></i> &nbsp; Setup Template\n    </button>\n    <button mat-raised-button color=\"raised-warning\" (click)=\"esAdminService.updateVersion()\">\n      <i class=\"fas fa-arrow-up\"></i> &nbsp; Update Version\n    </button>\n  </div>\n\n</div>\n",
                    styles: [".es-entity-admin-menu-container{margin-top:40px}.es-entity-admin-menu-container h2{color:#000;border-bottom:none}.es-entity-admin-menu-container .index-with-alias{font-weight:700;border-bottom:none}.es-entity-admin-button-row{text-align:center}.es-entity-admin-button-row button{margin-right:5px}"]
                },] },
    ];
    /** @nocollapse */
    EsEntityAdminComponent.ctorParameters = function () { return []; };
    EsEntityAdminComponent.propDecorators = {
        esAdminService: [{ type: Input }],
        title: [{ type: Input }]
    };
    return EsEntityAdminComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var BootstartElasticsearchModule = /** @class */ (function () {
    function BootstartElasticsearchModule() {
    }
    BootstartElasticsearchModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        MatListModule,
                        MatButtonModule
                    ],
                    declarations: [
                        EsEntityAdminComponent
                    ],
                    exports: [
                        EsEntityAdminComponent
                    ]
                },] },
    ];
    return BootstartElasticsearchModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var DefaultEsAdminService = /** @class */ (function () {
    function DefaultEsAdminService(_http, baseURl, entityName) {
        this.http = _http;
        this.BASE_URL = baseURl;
        this.ENTITY_NAME = entityName;
    }
    /**
     * @return {?}
     */
    DefaultEsAdminService.prototype.refreshSummary = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.http.get(this.BASE_URL + 'indices-summary', {}).subscribe(function (response) { return _this.summary = response.sort(function (a, b) {
            return a.version - b.version;
        }); }, function () { return console.error('Unable to retrieve index summary for ' + _this.ENTITY_NAME); });
    };
    /**
     * @return {?}
     */
    DefaultEsAdminService.prototype.setupTemplate = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.http.post(this.BASE_URL + 'setup-index-template', {}, {}).subscribe(function () {
            _this.refreshSummary();
            console.info('Mapping template successfully setup for ' + _this.ENTITY_NAME);
        }, function () {
            console.error('Unable to setup mapping template for ' + _this.ENTITY_NAME);
        });
    };
    /**
     * @return {?}
     */
    DefaultEsAdminService.prototype.updateVersion = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.http.post(this.BASE_URL + 'update-index-version', {}, {}).subscribe(function () {
            console.info('Updated version of ' + _this.ENTITY_NAME + ' index');
            setTimeout(function () { return _this.refreshSummary(); }, 500);
        }, function () {
            console.error('Unable to update index version for ' + _this.ENTITY_NAME);
        });
    };
    /**
     * @param {?} target
     * @return {?}
     */
    DefaultEsAdminService.prototype.switchAlias = /**
     * @param {?} target
     * @return {?}
     */
    function (target) {
        var _this = this;
        this.http.post(this.BASE_URL + 'switch-index-alias', {}, {
            params: new HttpParams().append('esIndexName', target)
        }).subscribe(function () { return _this.refreshSummary(); }, function () {
            console.error('Unable to switch alias to index ' + target);
        });
    };
    /**
     * @param {?} target
     * @return {?}
     */
    DefaultEsAdminService.prototype.deleteIndex = /**
     * @param {?} target
     * @return {?}
     */
    function (target) {
        var _this = this;
        this.http.post(this.BASE_URL + 'delete-index', {}, {
            params: new HttpParams().append('esIndexName', target)
        }).subscribe(function () { return _this.refreshSummary(); }, function () {
            console.error('Unable to delete index ' + target);
        });
    };
    return DefaultEsAdminService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

export { BootstartElasticsearchModule, DefaultEsAdminService, EsEntityAdminComponent as ɵa };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2guanMubWFwIiwic291cmNlcyI6WyJuZzovL2Jvb3RzdGFydC1lbGFzdGljc2VhcmNoL2xpYi9jb21wb25lbnRzL2VzLWVudGl0eS1hZG1pbi9lcy1lbnRpdHktYWRtaW4uY29tcG9uZW50LnRzIiwibmc6Ly9ib290c3RhcnQtZWxhc3RpY3NlYXJjaC9saWIvYm9vdHN0YXJ0LWVsYXN0aWNzZWFyY2gubW9kdWxlLnRzIiwibmc6Ly9ib290c3RhcnQtZWxhc3RpY3NlYXJjaC9saWIvbW9kZWxzL2RlZmF1bHQtZXMtYWRtaW4uc2VydmljZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0VzQWRtaW5TZXJ2aWNlfSBmcm9tICcuLi8uLi9tb2RlbHMvZXMtYWRtaW4tc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Jvb3RzdGFydC1lcy1lbnRpdHktYWRtaW4nLFxuICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJlcy1lbnRpdHktYWRtaW4tbWVudS1jb250YWluZXJcIj5cbiAgPGgyPnt7dGl0bGV9fTwvaDI+XG5cbiAgPG1hdC1saXN0PlxuICAgIDxtYXQtbGlzdC1pdGVtICpuZ0Zvcj1cImxldCBpbmRleFN1bW1hcnkgb2YgZXNBZG1pblNlcnZpY2Uuc3VtbWFyeVwiIGNsYXNzPVwicm93XCJcbiAgICAgICAgICAgICAgICAgICBbbmdDbGFzc109XCJ7J2luZGV4LXdpdGgtYWxpYXMnOiBpbmRleFN1bW1hcnkud2l0aEFsaWFzfVwiPlxuICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS1hdXRvXCI+XG4gICAgICAgIDxpIGNsYXNzPVwiZmFzIGZhLXRhZ1wiICpuZ0lmPVwiaW5kZXhTdW1tYXJ5LndpdGhBbGlhc1wiPjwvaT4gJm5ic3A7XG4gICAgICAgIDxzcGFuPlZlcnNpb24ge3tpbmRleFN1bW1hcnkudmVyc2lvbn19PC9zcGFuPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLWF1dG9cIj5cbiAgICAgICAgPHNwYW4+e3tpbmRleFN1bW1hcnkuZG9jQ291bnR9fSBkb2N1bWVudHM8L3NwYW4+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxidXR0b24gbWF0LWJ1dHRvbiBjb2xvcj1cIndhcm5cIiBjbGFzcz1cImNvbC1zbS1hdXRvXCJcbiAgICAgICAgICAgICAgdGl0bGU9XCJEZWxldGUgaW5kZXhcIlxuICAgICAgICAgICAgICAoY2xpY2spPVwiZXNBZG1pblNlcnZpY2UuZGVsZXRlSW5kZXgoaW5kZXhTdW1tYXJ5Lm5hbWUpXCI+XG4gICAgICAgIDxpIGNsYXNzPVwiZmFzIGZhLXRyYXNoXCI+PC9pPlxuICAgICAgPC9idXR0b24+XG4gICAgICA8YnV0dG9uIG1hdC1idXR0b24gY29sb3I9XCJwcmltYXJ5XCJcbiAgICAgICAgICAgICAgY2xhc3M9XCJjb2wtc20tYXV0b1wiXG4gICAgICAgICAgICAgICpuZ0lmPVwiIWluZGV4U3VtbWFyeS53aXRoQWxpYXNcIlxuICAgICAgICAgICAgICB0aXRsZT1cIlN3aXRjaCBBbGlhc1wiXG4gICAgICAgICAgICAgIChjbGljayk9XCJlc0FkbWluU2VydmljZS5zd2l0Y2hBbGlhcyhpbmRleFN1bW1hcnkubmFtZSlcIj5cbiAgICAgICAgPGkgY2xhc3M9XCJmYXMgZmEtdGFnXCI+PC9pPlxuICAgICAgPC9idXR0b24+XG4gICAgPC9tYXQtbGlzdC1pdGVtPlxuICA8L21hdC1saXN0PlxuXG4gIDxkaXYgY2xhc3M9XCJlcy1lbnRpdHktYWRtaW4tYnV0dG9uLXJvd1wiPlxuICAgIDxidXR0b24gbWF0LXJhaXNlZC1idXR0b24gY29sb3I9XCJyYWlzZWQtaW5mb1wiIChjbGljayk9XCJlc0FkbWluU2VydmljZS5zZXR1cFRlbXBsYXRlKClcIj5cbiAgICAgIDxpIGNsYXNzPVwiZmFzIGZhLWJvb2tcIj48L2k+ICZuYnNwOyBTZXR1cCBUZW1wbGF0ZVxuICAgIDwvYnV0dG9uPlxuICAgIDxidXR0b24gbWF0LXJhaXNlZC1idXR0b24gY29sb3I9XCJyYWlzZWQtd2FybmluZ1wiIChjbGljayk9XCJlc0FkbWluU2VydmljZS51cGRhdGVWZXJzaW9uKClcIj5cbiAgICAgIDxpIGNsYXNzPVwiZmFzIGZhLWFycm93LXVwXCI+PC9pPiAmbmJzcDsgVXBkYXRlIFZlcnNpb25cbiAgICA8L2J1dHRvbj5cbiAgPC9kaXY+XG5cbjwvZGl2PlxuYCxcbiAgc3R5bGVzOiBbYC5lcy1lbnRpdHktYWRtaW4tbWVudS1jb250YWluZXJ7bWFyZ2luLXRvcDo0MHB4fS5lcy1lbnRpdHktYWRtaW4tbWVudS1jb250YWluZXIgaDJ7Y29sb3I6IzAwMDtib3JkZXItYm90dG9tOm5vbmV9LmVzLWVudGl0eS1hZG1pbi1tZW51LWNvbnRhaW5lciAuaW5kZXgtd2l0aC1hbGlhc3tmb250LXdlaWdodDo3MDA7Ym9yZGVyLWJvdHRvbTpub25lfS5lcy1lbnRpdHktYWRtaW4tYnV0dG9uLXJvd3t0ZXh0LWFsaWduOmNlbnRlcn0uZXMtZW50aXR5LWFkbWluLWJ1dHRvbi1yb3cgYnV0dG9ue21hcmdpbi1yaWdodDo1cHh9YF1cbn0pXG5leHBvcnQgY2xhc3MgRXNFbnRpdHlBZG1pbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgZXNBZG1pblNlcnZpY2U6IEVzQWRtaW5TZXJ2aWNlO1xuICBASW5wdXQoKSB0aXRsZTogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5lc0FkbWluU2VydmljZS5yZWZyZXNoU3VtbWFyeSgpO1xuICB9XG59XG4iLCJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7RXNFbnRpdHlBZG1pbkNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2VzLWVudGl0eS1hZG1pbi9lcy1lbnRpdHktYWRtaW4uY29tcG9uZW50JztcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtNYXRCdXR0b25Nb2R1bGUsIE1hdExpc3RNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcblxuQE5nTW9kdWxlKHtcbiAgICAgICAgICAgIGltcG9ydHMgICAgIDogW1xuICAgICAgICAgICAgICBDb21tb25Nb2R1bGUsXG4gICAgICAgICAgICAgIE1hdExpc3RNb2R1bGUsXG4gICAgICAgICAgICAgIE1hdEJ1dHRvbk1vZHVsZVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGRlY2xhcmF0aW9uczogW1xuICAgICAgICAgICAgICBFc0VudGl0eUFkbWluQ29tcG9uZW50XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZXhwb3J0cyAgICAgOiBbXG4gICAgICAgICAgICAgIEVzRW50aXR5QWRtaW5Db21wb25lbnRcbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEVsYXN0aWNzZWFyY2hNb2R1bGUge1xufVxuIiwiaW1wb3J0IHtIdHRwQ2xpZW50LCBIdHRwUGFyYW1zfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQge0VzQWRtaW5TZXJ2aWNlfSBmcm9tICcuL2VzLWFkbWluLXNlcnZpY2UnO1xuaW1wb3J0IHtFc0luZGV4U3VtbWFyeX0gZnJvbSAnLi9lcy1pbmRleC1zdW1tYXJ5JztcblxuXG5leHBvcnQgY2xhc3MgRGVmYXVsdEVzQWRtaW5TZXJ2aWNlIGltcGxlbWVudHMgRXNBZG1pblNlcnZpY2Uge1xuXG4gIHByaXZhdGUgaHR0cDogSHR0cENsaWVudDtcbiAgQkFTRV9VUkw6IHN0cmluZztcbiAgRU5USVRZX05BTUU6IHN0cmluZztcbiAgc3VtbWFyeTogRXNJbmRleFN1bW1hcnlbXTtcblxuICBjb25zdHJ1Y3RvcihfaHR0cDogSHR0cENsaWVudCwgYmFzZVVSbDogc3RyaW5nLCBlbnRpdHlOYW1lOiBzdHJpbmcpIHtcbiAgICB0aGlzLmh0dHAgPSBfaHR0cDtcbiAgICB0aGlzLkJBU0VfVVJMID0gYmFzZVVSbDtcbiAgICB0aGlzLkVOVElUWV9OQU1FID0gZW50aXR5TmFtZTtcbiAgfVxuXG5cbiAgcHVibGljIHJlZnJlc2hTdW1tYXJ5KCk6IHZvaWQge1xuICAgIHRoaXMuaHR0cC5nZXQ8RXNJbmRleFN1bW1hcnlbXT4oXG4gICAgICB0aGlzLkJBU0VfVVJMICsgJ2luZGljZXMtc3VtbWFyeScsXG4gICAgICB7fVxuICAgICkuc3Vic2NyaWJlKFxuICAgICAgcmVzcG9uc2UgPT4gdGhpcy5zdW1tYXJ5ID0gcmVzcG9uc2Uuc29ydChmdW5jdGlvbiAoYSwgYikge1xuICAgICAgICByZXR1cm4gYS52ZXJzaW9uIC0gYi52ZXJzaW9uO1xuICAgICAgfSksXG4gICAgICAoKSA9PiBjb25zb2xlLmVycm9yKCdVbmFibGUgdG8gcmV0cmlldmUgaW5kZXggc3VtbWFyeSBmb3IgJyArIHRoaXMuRU5USVRZX05BTUUpXG4gICAgKTtcbiAgfVxuXG4gIHB1YmxpYyBzZXR1cFRlbXBsYXRlKCk6IHZvaWQge1xuICAgIHRoaXMuaHR0cC5wb3N0KFxuICAgICAgdGhpcy5CQVNFX1VSTCArICdzZXR1cC1pbmRleC10ZW1wbGF0ZScsXG4gICAgICB7fSxcbiAgICAgIHt9XG4gICAgKS5zdWJzY3JpYmUoXG4gICAgICAoKSA9PiB7XG4gICAgICAgIHRoaXMucmVmcmVzaFN1bW1hcnkoKTtcbiAgICAgICAgY29uc29sZS5pbmZvKCdNYXBwaW5nIHRlbXBsYXRlIHN1Y2Nlc3NmdWxseSBzZXR1cCBmb3IgJyArIHRoaXMuRU5USVRZX05BTUUpO1xuICAgICAgfSxcbiAgICAgICgpID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcignVW5hYmxlIHRvIHNldHVwIG1hcHBpbmcgdGVtcGxhdGUgZm9yICcgKyB0aGlzLkVOVElUWV9OQU1FKTtcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbiAgcHVibGljIHVwZGF0ZVZlcnNpb24oKTogdm9pZCB7XG4gICAgdGhpcy5odHRwLnBvc3QoXG4gICAgICB0aGlzLkJBU0VfVVJMICsgJ3VwZGF0ZS1pbmRleC12ZXJzaW9uJyxcbiAgICAgIHt9LFxuICAgICAge31cbiAgICApLnN1YnNjcmliZShcbiAgICAgICgpID0+IHtcbiAgICAgICAgY29uc29sZS5pbmZvKCdVcGRhdGVkIHZlcnNpb24gb2YgJyArIHRoaXMuRU5USVRZX05BTUUgKyAnIGluZGV4Jyk7XG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4gdGhpcy5yZWZyZXNoU3VtbWFyeSgpLCA1MDApO1xuICAgICAgfSxcbiAgICAgICgpID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcignVW5hYmxlIHRvIHVwZGF0ZSBpbmRleCB2ZXJzaW9uIGZvciAnICsgdGhpcy5FTlRJVFlfTkFNRSk7XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIHB1YmxpYyBzd2l0Y2hBbGlhcyh0YXJnZXQ6IHN0cmluZykge1xuICAgIHRoaXMuaHR0cC5wb3N0KFxuICAgICAgdGhpcy5CQVNFX1VSTCArICdzd2l0Y2gtaW5kZXgtYWxpYXMnLFxuICAgICAge30sXG4gICAgICB7XG4gICAgICAgIHBhcmFtczogbmV3IEh0dHBQYXJhbXMoKS5hcHBlbmQoJ2VzSW5kZXhOYW1lJywgdGFyZ2V0KVxuICAgICAgfVxuICAgICkuc3Vic2NyaWJlKFxuICAgICAgKCkgPT4gdGhpcy5yZWZyZXNoU3VtbWFyeSgpLFxuICAgICAgKCkgPT4ge1xuICAgICAgICBjb25zb2xlLmVycm9yKCdVbmFibGUgdG8gc3dpdGNoIGFsaWFzIHRvIGluZGV4ICcgKyB0YXJnZXQpO1xuICAgICAgfVxuICAgICk7XG4gIH1cblxuICBwdWJsaWMgZGVsZXRlSW5kZXgodGFyZ2V0OiBzdHJpbmcpIHtcbiAgICB0aGlzLmh0dHAucG9zdChcbiAgICAgIHRoaXMuQkFTRV9VUkwgKyAnZGVsZXRlLWluZGV4JyxcbiAgICAgIHt9LFxuICAgICAge1xuICAgICAgICBwYXJhbXM6IG5ldyBIdHRwUGFyYW1zKCkuYXBwZW5kKCdlc0luZGV4TmFtZScsIHRhcmdldClcbiAgICAgIH1cbiAgICApLnN1YnNjcmliZShcbiAgICAgICgpID0+IHRoaXMucmVmcmVzaFN1bW1hcnkoKSxcbiAgICAgICgpID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcignVW5hYmxlIHRvIGRlbGV0ZSBpbmRleCAnICsgdGFyZ2V0KTtcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbn1cbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTtJQW1ERTtLQUNDOzs7O0lBRUQseUNBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztLQUN0Qzs7Z0JBckRGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxRQUFRLEVBQUUsdy9DQXNDWDtvQkFDQyxNQUFNLEVBQUUsQ0FBQywwU0FBMFMsQ0FBQztpQkFDclQ7Ozs7O2lDQUdFLEtBQUs7d0JBQ0wsS0FBSzs7aUNBakRSOzs7Ozs7O0FDQUE7Ozs7Z0JBS0MsUUFBUSxTQUFDO29CQUNFLE9BQU8sRUFBTzt3QkFDWixZQUFZO3dCQUNaLGFBQWE7d0JBQ2IsZUFBZTtxQkFDaEI7b0JBQ0QsWUFBWSxFQUFFO3dCQUNaLHNCQUFzQjtxQkFDdkI7b0JBQ0QsT0FBTyxFQUFPO3dCQUNaLHNCQUFzQjtxQkFDdkI7aUJBQ0Y7O3VDQWpCWDs7Ozs7OztBQ0FBLElBS0E7SUFPRSwrQkFBWSxLQUFpQixFQUFFLE9BQWUsRUFBRSxVQUFrQjtRQUNoRSxJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztRQUNsQixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztRQUN4QixJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQztLQUMvQjs7OztJQUdNLDhDQUFjOzs7OztRQUNuQixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FDWCxJQUFJLENBQUMsUUFBUSxHQUFHLGlCQUFpQixFQUNqQyxFQUFFLENBQ0gsQ0FBQyxTQUFTLENBQ1QsVUFBQSxRQUFRLElBQUksT0FBQSxLQUFJLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQztZQUNyRCxPQUFPLENBQUMsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQztTQUM5QixDQUFDLEdBQUEsRUFDRixjQUFNLE9BQUEsT0FBTyxDQUFDLEtBQUssQ0FBQyx1Q0FBdUMsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUEsQ0FDaEYsQ0FBQzs7Ozs7SUFHRyw2Q0FBYTs7Ozs7UUFDbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQ1osSUFBSSxDQUFDLFFBQVEsR0FBRyxzQkFBc0IsRUFDdEMsRUFBRSxFQUNGLEVBQUUsQ0FDSCxDQUFDLFNBQVMsQ0FDVDtZQUNFLEtBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN0QixPQUFPLENBQUMsSUFBSSxDQUFDLDBDQUEwQyxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztTQUM3RSxFQUNEO1lBQ0UsT0FBTyxDQUFDLEtBQUssQ0FBQyx1Q0FBdUMsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7U0FDM0UsQ0FDRixDQUFDOzs7OztJQUdHLDZDQUFhOzs7OztRQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDWixJQUFJLENBQUMsUUFBUSxHQUFHLHNCQUFzQixFQUN0QyxFQUFFLEVBQ0YsRUFBRSxDQUNILENBQUMsU0FBUyxDQUNUO1lBQ0UsT0FBTyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxDQUFDO1lBQ2xFLFVBQVUsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLGNBQWMsRUFBRSxHQUFBLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDOUMsRUFDRDtZQUNFLE9BQU8sQ0FBQyxLQUFLLENBQUMscUNBQXFDLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ3pFLENBQ0YsQ0FBQzs7Ozs7O0lBR0csMkNBQVc7Ozs7Y0FBQyxNQUFjOztRQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDWixJQUFJLENBQUMsUUFBUSxHQUFHLG9CQUFvQixFQUNwQyxFQUFFLEVBQ0Y7WUFDRSxNQUFNLEVBQUUsSUFBSSxVQUFVLEVBQUUsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQztTQUN2RCxDQUNGLENBQUMsU0FBUyxDQUNULGNBQU0sT0FBQSxLQUFJLENBQUMsY0FBYyxFQUFFLEdBQUEsRUFDM0I7WUFDRSxPQUFPLENBQUMsS0FBSyxDQUFDLGtDQUFrQyxHQUFHLE1BQU0sQ0FBQyxDQUFDO1NBQzVELENBQ0YsQ0FBQzs7Ozs7O0lBR0csMkNBQVc7Ozs7Y0FBQyxNQUFjOztRQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDWixJQUFJLENBQUMsUUFBUSxHQUFHLGNBQWMsRUFDOUIsRUFBRSxFQUNGO1lBQ0UsTUFBTSxFQUFFLElBQUksVUFBVSxFQUFFLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxNQUFNLENBQUM7U0FDdkQsQ0FDRixDQUFDLFNBQVMsQ0FDVCxjQUFNLE9BQUEsS0FBSSxDQUFDLGNBQWMsRUFBRSxHQUFBLEVBQzNCO1lBQ0UsT0FBTyxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsR0FBRyxNQUFNLENBQUMsQ0FBQztTQUNuRCxDQUNGLENBQUM7O2dDQTFGTjtJQTZGQzs7Ozs7Ozs7Ozs7Ozs7In0=